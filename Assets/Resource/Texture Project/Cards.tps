<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/Nguyen/Desktop/Desktop/Solitaire/Assets/Resource/Texture Project/Cards.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../SpritesPackaged/Cards.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../Sprites/Resouce_I/1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,6,5</rect>
                <key>scale9Paddings</key>
                <rect>3,2,6,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/Button_Hints.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,28,47,56</rect>
                <key>scale9Paddings</key>
                <rect>24,28,47,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/Button_Undo.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,26,49,52</rect>
                <key>scale9Paddings</key>
                <rect>25,26,49,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/Card/10_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/10_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/10_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/10_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/2_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/2_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/2_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/2_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/3_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/3_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/3_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/3_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/4_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/4_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/4_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/4_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/5_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/5_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/5_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/5_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/6_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/6_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/6_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/6_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/7_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/7_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/7_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/7_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/8_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/8_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/8_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/8_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/9_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/9_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/9_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/9_Spade.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/A_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/A_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/A_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/A_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/Card_Back.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/J_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/J_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/J_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/J_Spade .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/K_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/K_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/K_Hearts.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/K_Spade.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/Q_Clubs.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/Q_Diamonds.png</key>
            <key type="filename">../Sprites/Resouce_I/Card/Q_Hearts .png</key>
            <key type="filename">../Sprites/Resouce_I/Card/Q_Spade.png</key>
            <key type="filename">../Sprites/Resouce_I/dialog_card.png</key>
            <key type="filename">../Sprites/Resouce_I/reload_card.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,69,118,139</rect>
                <key>scale9Paddings</key>
                <rect>59,69,118,139</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/ChangeTheme_Pack1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,35,65,70</rect>
                <key>scale9Paddings</key>
                <rect>32,35,65,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/LightCard.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>65,78,130,157</rect>
                <key>scale9Paddings</key>
                <rect>65,78,130,157</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/NoAds.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,34,64,68</rect>
                <key>scale9Paddings</key>
                <rect>32,34,64,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/Number_Hints.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/button_music off.png</key>
            <key type="filename">../Sprites/Resouce_I/button_music on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,27,56,55</rect>
                <key>scale9Paddings</key>
                <rect>28,27,56,55</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/button_restart.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,26,48,53</rect>
                <key>scale9Paddings</key>
                <rect>24,26,48,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/button_sound off.png</key>
            <key type="filename">../Sprites/Resouce_I/button_sound on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,28,64,56</rect>
                <key>scale9Paddings</key>
                <rect>32,28,64,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../Sprites/Resouce_I/dialog_score.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,21,188,43</rect>
                <key>scale9Paddings</key>
                <rect>94,21,188,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../Sprites/Resouce_I/Card/2_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/9_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/9_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/9_Spade.png</filename>
            <filename>../Sprites/Resouce_I/Card/10_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/10_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/10_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/10_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/A_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/A_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/A_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/A_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/Card_Back.png</filename>
            <filename>../Sprites/Resouce_I/Card/J_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/J_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/J_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/J_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/K_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/K_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/K_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/K_Spade.png</filename>
            <filename>../Sprites/Resouce_I/Card/Q_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/Q_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/Q_Hearts .png</filename>
            <filename>../Sprites/Resouce_I/Card/Q_Spade.png</filename>
            <filename>../Sprites/Resouce_I/Card/2_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/2_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/2_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/3_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/3_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/3_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/3_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/4_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/4_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/4_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/4_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/5_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/5_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/5_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/5_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/6_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/6_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/6_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/6_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/7_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/7_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/7_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/7_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/8_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Card/8_Diamonds.png</filename>
            <filename>../Sprites/Resouce_I/Card/8_Hearts.png</filename>
            <filename>../Sprites/Resouce_I/Card/8_Spade .png</filename>
            <filename>../Sprites/Resouce_I/Card/9_Clubs.png</filename>
            <filename>../Sprites/Resouce_I/Button_Hints.png</filename>
            <filename>../Sprites/Resouce_I/dialog_card.png</filename>
            <filename>../Sprites/Resouce_I/button_sound off.png</filename>
            <filename>../Sprites/Resouce_I/Button_Undo.png</filename>
            <filename>../Sprites/Resouce_I/button_sound on.png</filename>
            <filename>../Sprites/Resouce_I/button_restart.png</filename>
            <filename>../Sprites/Resouce_I/ChangeTheme_Pack1.png</filename>
            <filename>../Sprites/Resouce_I/LightCard.png</filename>
            <filename>../Sprites/Resouce_I/button_music off.png</filename>
            <filename>../Sprites/Resouce_I/button_music on.png</filename>
            <filename>../Sprites/Resouce_I/1.png</filename>
            <filename>../Sprites/Resouce_I/NoAds.png</filename>
            <filename>../Sprites/Resouce_I/reload_card.png</filename>
            <filename>../Sprites/Resouce_I/dialog_score.png</filename>
            <filename>../Sprites/Resouce_I/Number_Hints.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
