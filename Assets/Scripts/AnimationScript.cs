﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.6f).SetLoops(-1, LoopType.Yoyo);
	}
}
