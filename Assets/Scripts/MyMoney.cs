﻿using UnityEngine;
using UnityEngine.Advertisements;

public class MyMoney : Singleton<MyMoney>
{
	private System.Action<bool> rewardUnityCallback;

	// Unity ads
	private string
	androidGameId = "1562950",
	iosGameId = "1562950";

	void Start()
	{
		string gameId = null;
		#if UNITY_ANDROID
		gameId = androidGameId;
		#elif UNITY_IOS
		gameId = iosGameId;
		#endif

		if (Advertisement.isSupported && !Advertisement.isInitialized) {
			Advertisement.Initialize(gameId, false);
		}
	}

	public void HideBanner()
	{
		// disable banner for now
		return;
	}

	public void ShowBanner()
	{
	}

	public bool IsAdsReady()
	{
		if (Advertisement.IsReady()) return true;

		return false;
	}

	public bool ShowAd()
	{
		if (Advertisement.IsReady())
		{
			Advertisement.Show();
			return true;
		}

		return false;
	}



	public void ShowRewardAds(System.Action<bool> callback)
	{
		if (Advertisement.IsReady("rewardedVideo"))
		{
			var options = new ShowOptions { resultCallback = RewardUnityHandle };
			Advertisement.Show("rewardedVideo", options);
			rewardUnityCallback = callback;
			return;
		}
		callback(false);
	}

	void RewardUnityHandle(ShowResult result) {
		switch (result) {
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			if (rewardUnityCallback != null) {
				rewardUnityCallback (true);
			}
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			if (rewardUnityCallback != null) {
				rewardUnityCallback (false);
			}
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			if (rewardUnityCallback != null) {
				rewardUnityCallback (false);
			}
			break;
		}
	}
}