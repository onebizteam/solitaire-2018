﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

/// <summary>
/// Controller everything about touch.
/// </summary>
public class TouchEventSystem : Singleton < TouchEventSystem > {

    protected bool IsWaitingPress;
	public GameObject touchEffect;
	public float touchTime = 0;
	float shuffleTimer = 0;

    #region Button Function

    /// <summary>
    /// Restart the game.
    /// </summary>
    public void TouchRestartGame()
    {
		TouchEventSystem.Instance.JustTouch();
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
		AdSystem.Instance.ShowInterstitialAd();

        DialogSystem.Instance.ShowYesNo("Solitaire Kings", "Do you want to restart the game.", () =>
        {

            DoRestartGame();

        });

    }

	public void TouchShuffle()
	{
		

		TouchEventSystem.Instance.JustTouch();
		if (shuffleTimer > 0) return;
		if (GameManager.Instance.IsGameReady())
		{
			shuffleTimer = 1f;
			if (SupportManager.Instance.IsHaveShuffle())
			//if (true)
			{
				SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
				PoolSystem.Instance.ShuffleCards();
				HudSystem.Instance.StopHightlightShuffle();
			}
			else
			{
				DialogSystem.Instance.ShowDialogReward("shuffle");
			}
		}


		//PlayingCardsManager.Instance.ShuffleCard();
	}

	public void JustTouch()
	{
		touchTime = 0.2f;
		SupportManager.Instance.OnHaveAction();
	}

	void Update()
	{	
		if (touchTime > 0) touchTime -= Time.deltaTime;

		// Touch began
		if (Input.GetMouseButtonDown(0) && touchTime <= 0)
		{
			//Debug.Log("Mouse down");

		}

		if (Input.GetMouseButtonUp(0) && touchTime <= 0)
		{
			//Debug.Log("Mouse up");// Get mouse
			Vector3 screenPos = Input.mousePosition;

			screenPos = Camera.main.ScreenToWorldPoint(screenPos);
			screenPos.z = 0;
			Instantiate(touchEffect, screenPos, Quaternion.identity);
			SupportManager.Instance.OnHaveAction();
		}

		if (shuffleTimer > 0) shuffleTimer -= Time.deltaTime;

	}

    public void DoRestartGame()
    {   
        GamePlay.Instance.StopAllCoroutine();

        LoadingBehaviour.Instance.OnStartLoading = () =>
        {
            List<CardBehaviour> cardFound =  new List<CardBehaviour>( PoolSystem.Instance.GetAllCards());

            for ( int i = 0; i < cardFound.Count; i++)
            {
                PoolSystem.Instance.ReturnToPool(cardFound[i]);

                cardFound[i].UnlockCard(false);
            }
        };

		SoundSystems.Instance.StopMusic ();
    
        LoadingBehaviour.Instance.ShowLoading("PlayingScreen" , false,  false);  
    }

	public void GoSelectLevel()
	{   
		GamePlay.Instance.StopAllCoroutine();

		LoadingBehaviour.Instance.ShowLoading("LevelScreen" , false,  false);  
	}

    /// <summary>
    /// Show hint for game with the condition.
    /// </summary>
    public void TouchHintGame()
    {
		TouchEventSystem.Instance.JustTouch();
        if (GameManager.Instance.IsGameReady())
        {	
			if (SupportManager.Instance.IsHaveHint())
			{
				SoundSystems.Instance.PlaySound(Enums.SoundIndex.Press);

				GamePlay.Instance.ShowHintGame();

				HudSystem.Instance.StopHightlightHint();
			}
			else
			{
				DialogSystem.Instance.ShowDialogReward("hint");
			}
        }
    }

	/// <summary>
	/// Dos the show dialog themes.
	/// </summary>
	public void DoShowDialogThemes()
	{
		DialogSystem.Instance.ShowDialogThemes ();
	}

	/// <summary>
	/// Touchs the show menu settings.
	/// </summary>
	/// <param name="animation">Animation.</param>
    public void TouchShowMenuSettings(Animator animation)
    {
		TouchEventSystem.Instance.JustTouch();
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

		if (animation.GetBool ("IsAppear") == false) {

			animation.SetBool ("IsAppear", true);
		}
    }

	/// <summary>
	/// Touchs the hide menu settings.
	/// </summary>
	/// <param name="animation">Animation.</param>
    public void TouchHideMenuSettings(Animator animation)
	{

		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press); 

		if (animation.GetBool ("IsAppear") == true) {
			
			animation.SetBool ("IsAppear", false);
		}
    }

	/// <summary>
	/// Touchs the undo.
	/// </summary>
    public void TouchUndo()
    {
		TouchEventSystem.Instance.JustTouch();

        if (!GameManager.Instance.IsGameReady())
            return;

		if (SupportManager.Instance.IsHaveUndo())
		{
			SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
			GamePlay.Instance.DisableHintGame ();
			UndoSystem.Instance.UndoState ();

		}
		else
		{
			DialogSystem.Instance.ShowDialogReward("undo");
		}

    }

	/// <summary>
	/// Touchs the show hint cards.
	/// </summary>
    public void TouchShowHintCards()
    {
		JustTouch();
		GameManager.Instance.isHaveAction = true;

        if (GameManager.Instance.IsGameReady())
        {
			SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
			HidenCardsManager.Instance.DoShowingLockedCards();
        }
    }

	/// <summary>
	/// Enables the game object.
	/// </summary>
	/// <param name="param">Parameter.</param>
    public void EnableGameObject(GameObject param)
    {
        param.gameObject.SetActive(true);
    }

	/// <summary>
	/// Disables the game object.
	/// </summary>
	/// <param name="param">Parameter.</param>
    public void DisableGameObject( GameObject param)
    {
        param.gameObject.SetActive(false);
    }

	/// <summary>
	/// Touchs the dialog options.
	/// </summary>
	public void TouchDialogOptions()
	{
		TouchEventSystem.Instance.JustTouch();
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

		DialogSystem.Instance.ShowDialogOptions ();
	}

	public void TouchWatchVideo()
	{
		TouchEventSystem.Instance.JustTouch();
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
		DialogSystem.Instance.ShowDialogReward("none");
	}

	/// <summary>
	/// Touchs the game hard.
	/// </summary>
	public void TouchGameHard()
	{
        SoundSystems.Instance.PlaySound(Enums.SoundIndex.Press);

        GameManager.Instance.DifficultLevel = Enums.DifficultLevel.Hard;	
		LevelLoader.Instance.difficultLevel = Enums.DifficultLevel.Hard;

		SoundSystems.Instance.StopMusic ();
	}

	/// <summary>
	/// Touchs the game easy.
	/// </summary>
	public void TouchGameEasy()
	{
		if (GameManager.Instance.DifficultLevel != Enums.DifficultLevel.None)
			return;

        SoundSystems.Instance.PlaySound(Enums.SoundIndex.Press);

        GameManager.Instance.DifficultLevel = Enums.DifficultLevel.Easy;
		LevelLoader.Instance.difficultLevel = Enums.DifficultLevel.Easy;

		SoundSystems.Instance.StopMusic ();
	}

	/// <summary>
	/// Touchs the disable canvas group.
	/// </summary>
	/// <param name="canvas">Canvas.</param>
	public void TouchDisableCanvasGroup(CanvasGroup canvas)
	{
		canvas.DOFade (0, Contains.DurationFade).OnComplete (() => {

			canvas.gameObject.SetActive ( false );

			if ( GamePlay.Instance != null )
			{
				GamePlay.Instance.DisableBlur();
			}

		});
	}
#endregion

}
