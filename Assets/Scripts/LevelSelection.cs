﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class LevelSelection : MonoBehaviour {

	Image loadingFade;
	float loadingTime = 0.3f;
	[SerializeField] GameObject buttonPrefab;
	[SerializeField] RectTransform buttonHolder;
 	int row = 7;
	int column = 5;
	int paddingLeft = 113;
	int paddingTop = 200;
	int paddingBottom = 150;
	List<LevelButton> listButton;
	int NumberOfPage = 8;
	int LevelPerPage = 35;
	int currentPage = 0;

	[SerializeField] Image[] pageIndicator;
	[SerializeField] Sprite pageOn;
	[SerializeField] Sprite pageOff;
	float scaleFactor = 1;
	float touchStartPos = 0;
	float touchMovePos = 0;
	bool isHold = false;
	float holderY = 0;
	float lastHolderPosX = 0;
	float beganTouchTime = 0;

	public bool IsPortraitView {
		get { return Screen.width < Screen.height; }
	}

	public bool IsPortrait;

	void Awake()
	{
		loadingFade = GameObject.FindGameObjectWithTag("LoadingFade").GetComponent<Image>();
		loadingFade.color = Color.black;
		loadingFade.DOFade(0, loadingTime);
		holderY = buttonHolder.transform.position.y;
		lastHolderPosX = buttonHolder.transform.position.x;
	}

	void SetupButtons()
	{
		int buttonIndex = 0;

		for (int page = 0; page < NumberOfPage;  page++)
		{
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < column; j++)
				{
					float scale = 1;
					if (IsPortrait) scale = (float)Screen.width/720 * 0.85f;
					else scale = (float)Screen.width/1280 * 0.7f;
					Debug.Log("Scale = " + scale);
					buttonIndex = LevelPerPage * page + i * column + j + 1;
					GameObject button = Instantiate(buttonPrefab, Vector2.zero, Quaternion.identity) as GameObject;
					button.GetComponent<LevelButton>().buttonIndex = buttonIndex;

					button.transform.SetParent(buttonHolder);
					button.name = "Level" + buttonIndex.ToString("D3");
					button.transform.localScale = new Vector3(scale, scale, scale);
					listButton.Add(button.GetComponent<LevelButton>());
					button.GetComponent<LevelButton>().Init();

					if (button.GetComponent<LevelButton>().isUnlock)
					{
						int star = PlayerPrefs.GetInt("Level" + buttonIndex);
						//Debug.Log("Level " + buttonIndex + ": " + star);
						button.GetComponent<LevelButton>().SetStar(star);
					}
				}
			}
		}


	}

	void UpdateButtonPos()
	{
		holderY = buttonHolder.transform.position.y;
		Debug.Log("Screen = " + Screen.width + " - " + Screen.height);
		if (IsPortrait)
		{
			float scale = (float)Screen.width/720;
			row = 7;
			column = 5;
			paddingLeft = (int)(113 * scale);
			paddingTop = (int)(200 * scale);
			paddingBottom = (int)(150 * scale);

		}
		else
		{
			float scale = (float)Screen.width/1280;
			row = 5;
			column = 7;
			paddingLeft = (int)(150 * scale);
			paddingTop = (int)(170 * scale);
			paddingBottom = (int)(110 * scale);
			//Debug.Log("zzz " + Screen.width);

		}
		float deltaX = (Screen.width * scaleFactor - paddingLeft * 2)/(float)(column - 1);
		float deltaY = (Screen.height * scaleFactor - paddingBottom - paddingTop)/(float)(row - 1);
		float startX = paddingLeft - Screen.width * scaleFactor/2;
		float startY = -paddingTop + Screen.height * scaleFactor/2;
		Vector2 pos = new Vector2(startX, startY);

		int buttonIndex = 0;

		for (int page = 0; page < NumberOfPage;page ++)
		{
			for (int i = 0; i < row; i++)
			{
				pos.x = startX + page * Screen.width * scaleFactor;
				for (int j = 0; j < column; j++)
				{
					buttonIndex = LevelPerPage * page + i * column + j;
					LevelButton button = listButton[buttonIndex];
					button.transform.localPosition = pos;

					pos.x += deltaX;
				}

				pos.y -= deltaY;
			}
			pos.y = startY;
		}

		MoveToPage(currentPage, 0);

	}

	// Use this for initialization
	void Start () {

		if (!IsPortrait && IsPortraitView) {

			IsPortrait = true;

		} else if ( IsPortrait && !IsPortraitView ) {

			IsPortrait = false;
		}


		listButton = new List<LevelButton>();
		SetupButtons();
		UpdateButtonPos();

		int page = LevelLoader.Instance.latestLevel/35;
		MoveToPage(page, 0);
	}
	
	// Update is called once per frame
	void Update () {

		if (!IsPortrait && IsPortraitView) {

			IsPortrait = true;

			UpdateButtonPos ();

		} else if ( IsPortrait && !IsPortraitView ) {

			IsPortrait = false;

			UpdateButtonPos ();
		}

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			Vector3 touchScreen = touch.position;

			if (touch.phase == TouchPhase.Began) 
			{
				OnTouchBegan(touchScreen);
			}
			else if (touch.phase == TouchPhase.Moved) 
			{
				OnTouchMoved(touchScreen);
			}			
			else if (touch.phase == TouchPhase.Ended)
			{
				OnTouchEnded(touchScreen);
			}
		}

		// Get mouse
		Vector3 screenPos = Input.mousePosition;

		// Touch began
		if (Input.GetMouseButtonDown(0))
		{
			OnTouchBegan(screenPos);
		}

		// Touch move
		if ( Input.GetMouseButton(0) )
		{
			OnTouchMoved(screenPos);
		}

		// Touch end
		if ( Input.GetMouseButtonUp(0) )
		{
			OnTouchEnded(screenPos);
		}


	}

	// Touch listener
	public void OnTouchBegan(Vector2 screenPos)
	{
		if (screenPos.y > Screen.height - paddingTop) return;
		Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
		//Debug.Log("touch began: " + screenPos);
		touchStartPos = screenPos.x;
		touchMovePos = screenPos.x;
		isHold = true;
		lastHolderPosX = buttonHolder.transform.position.x;
		beganTouchTime = Time.time;
	}

	// Touch listener
	public void OnTouchMoved(Vector2 screenPos)
	{
		if (!isHold) return;

		Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
		touchMovePos = screenPos.x;
		buttonHolder.transform.position = new Vector2(lastHolderPosX - touchStartPos + touchMovePos, holderY);
	}

	// Touch listener
	public void OnTouchEnded(Vector2 screenPos)
	{
		if (!isHold) return;

		//if (IsPointerOverUIObject()) return;
		Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
		isHold = false;
		//Debug.Log("touch began: " + buttonHolder.transform.position);
		float deltaMoveX = touchStartPos - touchMovePos;
		float deltaMoveTime = Time.time - beganTouchTime;

		int currentIndex = (int)(-(buttonHolder.transform.localPosition.x / Screen.width));
		currentIndex = (int)Mathf.Clamp(currentIndex, 0, 6);
		float speed = deltaMoveX/deltaMoveTime/1000;
		if (speed > 0) currentIndex++;
		MoveToPage(currentIndex);
		//Debug.Log("Touch end: " + currentIndex + " - " + speed);
	}


	public void BackToHome()
	{
		loadingFade.DOFade(1, loadingTime);
		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("MenuScreen");
		});
	}

	public void LoadToInGame(int level)
	{
		Debug.Log("Load game " + level);
		LevelLoader.Instance.LoadLevel(level);
		loadingFade.DOFade(1, loadingTime);
		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("PlayingScreen");
		});
	}

	public void OnPageButton(int pageIndex)
	{
		MoveToPage(pageIndex);
		currentPage = pageIndex;
	}

	public void OnButtonNext()
	{
		if (currentPage < NumberOfPage - 1)
		MoveToPage(++currentPage);
	}

	public void OnButtonBack()
	{
		if (currentPage >= 1)
		MoveToPage(--currentPage);
		else
		{
			BackToHome();
		}
	}

	public void MoveToPage(int pageIndex, float moveTime = 0.5f)
	{
		currentPage = pageIndex;
		buttonHolder.DOLocalMoveX(-Screen.width * scaleFactor * (pageIndex), moveTime);
		DOVirtual.DelayedCall(moveTime, () => {
			UpdatePageIndicator();
		});

	}

	public void UpdatePageIndicator()
	{
		for (int i = 0; i < pageIndicator.Length; i++)
		{
			if (currentPage == i) pageIndicator[i].sprite = pageOn;
			else pageIndicator[i].sprite = pageOff;
		}
	}

	private bool IsPointerOverUIObject() {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		#if UNITY_EDITOR
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		#else
		eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
		#endif
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

		if (results.Count > 0)
		{
			string name = results[0].gameObject.name;
			//Debug.Log("Raycast UI: " + name);
		}

		return results.Count > 0;
	}
}
