﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;

public class SupportManager : Singleton<SupportManager> {

	public int StartHintNumber = 5;
	public int StartUndoNumber = 10;
	public int StartShuffleNumber = 5;
	public int SupporMaxNumber = 20;
	public int HintPerReward = 5;
	public int UndoPerReward = 10;
	public int ShufflePerReward = 5;
	float IdleTimeMax = 10;
	float idleTimer = 10;


	// Use this for initialization
	void Start () {
		if (!PlayerPrefs.HasKey("Undo")) PlayerPrefs.SetInt("Undo", StartUndoNumber);
		if (!PlayerPrefs.HasKey("Hint")) PlayerPrefs.SetInt("Hint", StartHintNumber);
		if (!PlayerPrefs.HasKey("Shuffle")) PlayerPrefs.SetInt("Shuffle", StartShuffleNumber);
		if (!PlayerPrefs.HasKey("RightHanded")) PlayerPrefs.SetInt("RightHanded", 1);
		UpdateSupportsUI();
	}

	public void UpdateSupportsUI()
	{
		GameObject.Find("ShuffleNumber").transform.Find("Text").GetComponent<Text>().text 
		 	= PlayerPrefs.GetInt("Shuffle").ToString();

		GameObject.Find("UndoNumber").transform.Find("Text").GetComponent<Text>().text 
			= PlayerPrefs.GetInt("Undo").ToString();

		GameObject.Find("HintNumber").transform.Find("Text").GetComponent<Text>().text 
			= PlayerPrefs.GetInt("Hint").ToString();
	}

	#if UNITY_EDITOR
	public class MenuItems
	{
		[MenuItem("Tools/Clear PlayerPrefs")]
		private static void NewMenuOption()
		{
			PlayerPrefs.DeleteAll();
			Debug.Log("Delete all data ...");
		}
	}
	#endif

	public bool IsHaveHint()
	{
		return (PlayerPrefs.GetInt("Hint") > 0);
	}

	public bool IsHaveUndo()
	{
		return (PlayerPrefs.GetInt("Undo") > 0);
	}

	public bool IsHaveShuffle()
	{
		return (PlayerPrefs.GetInt("Shuffle") > 0);
	}

	public void OnUseHint()
	{
		PlayerPrefs.SetInt("Hint", PlayerPrefs.GetInt("Hint") - 1);
		UpdateSupportsUI();
	}

	public void OnUseShuffle()
	{
		PlayerPrefs.SetInt("Shuffle", PlayerPrefs.GetInt("Shuffle") - 1);
		UpdateSupportsUI();
	}

	public void OnUseUndo()
	{
		PlayerPrefs.SetInt("Undo", PlayerPrefs.GetInt("Undo") - 1);
		UpdateSupportsUI();
	}

	public void RewardHint(int number, bool updateUI = true)
	{
		int hint = PlayerPrefs.GetInt("Hint") + number;
		if (hint > ExtraConfig.Instance.MaxHintNumber) hint = ExtraConfig.Instance.MaxHintNumber;
		PlayerPrefs.SetInt("Hint", hint);
		if (updateUI) UpdateSupportsUI();
	}

	public void RewardUndo(int number, bool updateUI = true)
	{
		int undo = PlayerPrefs.GetInt("Undo") + number;
		if (undo > ExtraConfig.Instance.MaxUndoNumber) undo = ExtraConfig.Instance.MaxUndoNumber;
		PlayerPrefs.SetInt("Undo", undo);
		if (updateUI) UpdateSupportsUI();
	}

	public void RewardShuffle(int number, bool updateUI = true)
	{
		int numberShuffle = PlayerPrefs.GetInt("Shuffle") + number;
		if (numberShuffle > ExtraConfig.Instance.MaxShuffleNumber) numberShuffle = ExtraConfig.Instance.MaxShuffleNumber;

		PlayerPrefs.SetInt("Shuffle", numberShuffle);
		if (updateUI) UpdateSupportsUI();
	}

	public void OnHaveAction()
	{
		idleTimer = IdleTimeMax;
		HudSystem.Instance.StopHightlightHint();
		Debug.Log("Have action ... ");

	}
	
	// Update is called once per frame
	void Update () {
		
		if (idleTimer > 0)
		{
			idleTimer -= Time.deltaTime;
		}
		else
		{
			idleTimer = IdleTimeMax;
			HudSystem.Instance.PlayHightlightHint();
		}
	}

}
