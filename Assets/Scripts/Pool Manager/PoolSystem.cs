﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Pool system.
/// </summary>
public class PoolSystem : Singleton < PoolSystem > {

	// ============================= References =========================== //

	#region References

	/// <summary>
	/// The item properties.
	/// </summary>
	[SerializeField]
	protected List < CardBehaviour > ItemProperties;

	#endregion

	#region Functional

	/// <summary>
	/// Gets all cards.
	/// </summary>
	/// <returns>The all cards.</returns>
	public List < CardBehaviour > GetAllCards()
	{
		return ItemProperties;
	}

	/// <summary>
	/// Clears the cards.
	/// </summary>
	public void ClearCards()
	{
		for (int i = 0; i < ItemProperties.Count; i++) {
			if (ItemProperties [i] != null) {
				Destroy (ItemProperties [i].gameObject);
			}
		}

		ItemProperties.Clear ();
	}

	/// <summary>
	/// Clear this instance.
	/// </summary>
    public void Clear()
    {
        ItemProperties.Clear();
    }

	/// <summary>
	/// Returns to pool.
	/// </summary>
	/// <param name="param">Parameter.</param>
	public void ReturnToPool(CardBehaviour param)
	{
		param.gameObject.SetActive (false);

		if ( ItemProperties.Contains ( param ))
		{
			LogGame.DebugLog (string.Format ( "[Pool System] Card Was Found {0}" , param.name));

            ItemProperties.Remove(param);
        }

		param.transform.SetParent (transform);

		ItemProperties.Add (param);
	}

	/// <summary>
	/// Gets the number of cards.
	/// </summary>
	public int GetNumberOfCards()
	{
        for (int i = 0; i < ItemProperties.Count; i++)
        {
            if (ItemProperties[i] == null)
            {
                ItemProperties.RemoveAt(i);
            }
        }

        return ItemProperties.Count;
	}

	public void CheckDuplicateCard()
	{
		CardBehaviour[] objs = GameObject.FindObjectsOfType<CardBehaviour>();
		Debug.Log("zzz =" + objs.Length);
		for (int i = 1; i < objs.Length; i++)
		{
			for (int j = 0; j < i; j++)
			{
				if (objs[i].GetDataCard().GetCardSprite().name.Equals(objs[j].GetDataCard().GetCardSprite().name))
				{
					Debug.Log("Duplicate! " + i + " - " + objs[i].GetDataCard().GetCardSprite().name);
					Debug.Log("Duplicate! " + j + " - " + objs[j].GetDataCard().GetCardSprite().name);
					Debug.Log("card1 =" + objs[i].GetDataCard().GetCardType() + " - " + objs[i].GetDataCard().GetEnumCardValue());
					Debug.Log("card2 =" + objs[j].GetDataCard().GetCardType() + " - " + objs[j].GetDataCard().GetEnumCardValue());
				}
			}
		}
	}

	public void ShuffleCards()
	{
		if (!HidenCardsManager.Instance.IsExistAnyCards()) return;

		HidenCardsManager.Instance.CloseAllCards();
		SupportManager.Instance.OnUseShuffle();
		
		DOVirtual.DelayedCall(0.2f, () => {
			CardBehaviour[] tempList = new CardBehaviour[52];
			int index = 0;
			int numberOfHidden = 0;
			for (int i = 0; i < 52; i++)
			{
				if (!ItemProperties[i].IsUnlocked() && ItemProperties[i].GetProperties().cardOnBoard == Enums.CardBoard.CardHint)
				{
					tempList[index] = ItemProperties[i];
					index++;
				}
			}
			numberOfHidden = index;
			for (int i = 0; i < 52; i++)
			{
				if (!ItemProperties[i].IsUnlocked() && ItemProperties[i].GetProperties().cardOnBoard == Enums.CardBoard.CardUse)
				{
					tempList[index] = ItemProperties[i];
					index++;
				}
			}

			// shuffle used card
			for (int i = numberOfHidden; i < ItemProperties.Count; i++) {
				var temp = ItemProperties[i];
				int randomIndex = Random.Range(i, ItemProperties.Count);
				ItemProperties[i] = ItemProperties[randomIndex];
				ItemProperties[randomIndex] = temp;
			}

			for (int i = 0; i < index/2; i++)
			{
				Helper.SwapCard(tempList[i], tempList[i + index/2]);
			}

			HidenCardsManager.Instance.UpdateHiddenUnlockCardList();
			PlayingCardsManager.Instance.UpdateListCardHolder();
		});

	}
	#endregion
}
