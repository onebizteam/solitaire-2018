﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeImage : MonoBehaviour {

	[SerializeField] Image imageSprite;
	[SerializeField] Sprite[] spriteList;

	// Use this for initialization
	void Start () {
		UpdateTheme();	
	}

	public void UpdateTheme()
	{
		if (Contains.CurrentStyle == Enums.Themes.Default) {
			imageSprite.sprite = spriteList[0];

		} else if (Contains.CurrentStyle == Enums.Themes.Candy) {
			imageSprite.sprite = spriteList[1];
		}
		else if (Contains.CurrentStyle == Enums.Themes.Style1) {
			imageSprite.sprite = spriteList[2];
		}
		else 
		{
			imageSprite.sprite = spriteList[3];
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
