﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraConfig : Singleton<ExtraConfig> {

	[Space(10)]	
	[Header("Reward Maximum Setting")]
	public int MaxHintNumber = 99;
	public int MaxShuffleNumber = 50;
	public int MaxUndoNumber = 99;

	[Space(10)]	
	[Header("Reward Get Setting")]
	public int HintPerReward = 5;
	public int UndoPerReward = 10;
	public int ShufflePerReward = 5;

	[Space(10)]	
	[Header("Reward Condition")]
	public int MinTimeToGetShuffle = 90;
	public int MinMoveToGetHint = 100;

	[Space(10)]	
	[Header("Level Difficult Setting")]
	public int EasyModeLevel = 50;
	public int NormalModeLevel = 100;
}
