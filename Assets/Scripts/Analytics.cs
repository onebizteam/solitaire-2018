﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analytics : Singleton<Analytics> {

	[SerializeField]
	private GoogleAnalyticsV4 analytics;

	void Start()
	{
		//analytics.bundleVersion = Application.version;
		LogScreen("Start Game");
	}

	public void LogScreen(string screenName) {
		Debug.Log("Analytics screen: " + screenName);
		if (analytics != null)
		analytics.LogScreen (screenName);
		
	}
}
