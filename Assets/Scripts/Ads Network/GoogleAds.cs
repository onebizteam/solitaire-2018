﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class GoogleAds : Singleton<GoogleAds> {

	private InterstitialAd interstitial = null;

	private BannerView banner = null;
	// Use this for initialization
	void Start () {
		LoadBanner ();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LoadBanner() {
		string adUnit = "ca-app-pub-8646484161678696/6294476773";

		banner = new BannerView (adUnit, AdSize.SmartBanner, AdPosition.Bottom);
		AdRequest request = new AdRequest.Builder ().Build ();
		banner.LoadAd(request);
		banner.Show ();
	}

	public void HideBanner() {
		//banner.Hide ();
	}

	public void ShowBanner() {
		return;
		//banner.Show ();
	}

	public void RequestInterstitial() {
		string adUnit = "ca-app-pub-8646484161678696/8164115734";

		interstitial = new InterstitialAd (adUnit);
		AdRequest request = new AdRequest.Builder ().Build ();
		interstitial.LoadAd (request);
	}

	public void ShowInterstitial() {
		return;
		if (interstitial != null && interstitial.IsLoaded ()) {
			interstitial.Show ();
		}

		RequestInterstitial ();
	}
}
