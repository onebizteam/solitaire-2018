﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

/// <summary>
/// Ad system.
/// </summary>
public class AdSystem : Singleton<AdSystem>
{
    // ================================ Action ============================= //
    #region Action
	/// <summary>
	/// The is use admob.
	/// </summary>
    public bool IsUseAdmob = true;

    #endregion

    #region Functional

    /// <summary>
    /// Awake this instance.
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        InitAdmobs();

		//ShowBanner();
    }		

	#endregion

    #region Admobs

    // =========================== References ======================== //

	/// <summary>
	/// The banner.
	/// </summary>
    BannerView banner;

	/// <summary>
	/// The interstitial ad.
	/// </summary>
    InterstitialAd interstitialAd;

	/// <summary>
	/// The banner android ad unit I.
	/// </summary>
    [Header("Admobs")]
    public string BannerAndroidAdUnitID = "INSERT_ANDROID_BANNER_AD_UNIT_ID_HERE";

	/// <summary>
	/// The banner IOS ad unit I.
	/// </summary>
    public string BannerIOSAdUnitID = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";

	/// /// <summary>
	/// The interstitial android ad unity I.
	/// </summary>
    public string InterstitialAndroidAdUnityID = "INSERT_ANDROID_INTERSTITIAL_AD_UNIT_ID_HERE";

	/// <summary>
	/// The interstitial IOS ad unity I.
	/// </summary>
    public string InterstitialIOSAdUnityID = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";

	public string RewardID = "";
	private System.Action<bool> rewardAdmobCallback;
	RewardBasedVideoAd admobVideo;

    // ========================== Init Admob =========================== //

	/// /// <summary>
	/// Inits the admobs.
	/// </summary>
    void InitAdmobs()
    {
		ResetInterstitial ();

		admobVideo = RewardBasedVideoAd.Instance;
		admobVideo.OnAdRewarded += AdmobRewardHandle;
		admobVideo.OnAdClosed += AdmobFailedHandle;
		admobVideo.LoadAd(new AdRequest.Builder().Build(), RewardID);
    }

	void RequestAdmobVideo()
	{
		admobVideo = RewardBasedVideoAd.Instance;
		admobVideo.LoadAd(new AdRequest.Builder().Build(), RewardID);
	}

	/// <summary>
	/// Shows the interstitial ad.
	/// </summary>
    public void ShowInterstitialAd()
    {
		if (Contains.IsHavingRemoveAd)
			return;

        if (!IsUseAdmob)
        {
            if (interstitialAd != null)
            {
                interstitialAd.Destroy();
            }

            return;
        }
            
        if ( interstitialAd != null && interstitialAd.IsLoaded() )
        {
            interstitialAd.Show();
        }
        else
        {
            RequestInterstitial();
        }
    }

	public bool ShowAdmobReward(System.Action<bool> callback)
	{
		if (admobVideo.IsLoaded())
		{
			admobVideo.Show();
			rewardAdmobCallback = callback;
			Debug.Log("Showing admob reward ...");
			return true;
		}
		else
		{
			Debug.Log("No Admob reward");

			return false;
		}
		RequestAdmobVideo();
	}

	public void ResetInterstitial(){
		if (interstitialAd != null && interstitialAd.IsLoaded ()) {
			return;
		}

		RequestInterstitial ();
	}

	/// <summary>
	/// Hides the interstitial ad.
	/// </summary>
    public void HideInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }

	/// <summary>
	/// Shows the banner.
	/// </summary>
    public void ShowBanner()
    {
		if (Contains.IsHavingRemoveAd)
			return;

        if (!IsUseAdmob)
        {
            if ( banner != null)
            {
                banner.Destroy();
            }

            return;
        }

        if ( banner != null)
        {
            banner.Show();
        } else
        {
            RequestBanner();
        }

//		string adUnit = "ca-app-pub-8646484161678696/6294476773";
//
//		banner = new BannerView (adUnit, AdSize.SmartBanner, AdPosition.Bottom);
//		AdRequest request = new AdRequest.Builder ().Build ();
//		banner.LoadAd(request);
//		banner.Show ();
    }

	void AdmobRewardHandle(object sender, Reward args) {
		Debug.Log ("Admob reward call back: " + args.ToString() + " - " + sender.ToString());
		if (rewardAdmobCallback != null) {
			rewardAdmobCallback (true);
		}
	}

	void AdmobFailedHandle(object sender, System.EventArgs args) {
		Debug.Log ("Admob reward call back failed ");
		if (rewardAdmobCallback != null) {
			rewardAdmobCallback (false);
		}
	}

	public void DestroyAndRequestBanner()
	{
		if (banner != null) {
			banner.Destroy ();
		}

		RequestBanner ();
	}

	/// <summary>
	/// Hides the banner.
	/// </summary>
    public void HideBanner()
    {
        if ( banner != null )
        {
			banner.Destroy();
        }
    }

	/// <summary>
	/// Requests the banner.
	/// </summary>
    private void RequestBanner()
	{
		if (Contains.IsHavingRemoveAd)
			return;

#if UNITY_EDITOR
		string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = BannerAndroidAdUnitID;
#elif UNITY_IOS
		string adUnitId = BannerIOSAdUnitID;
#else
        string adUnitId = "unexpected_platform";
#endif

		if (banner != null) {

			// Called when an ad request has successfully loaded.
			banner.OnAdLoaded -= Banner_OnAdLoaded;
			// Called when an ad request failed to load.
			banner.OnAdFailedToLoad -= Banner_OnAdFailedToLoad;

			// Called when the user returned from the app after an ad click.
			banner.OnAdClosed -= Banner_OnAdClosed;
		}


		IsAdLoading = false;


		if (MutilResolution.Instance != null) {
			if (MutilResolution.Instance.IsPortrait) {
				Debug.Log("build banner ... 1");
				// Create a 320x50 banner at the top of the screen.
				banner = new BannerView (adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

			} else {
		Debug.Log("build banner ... 2");
				if (Contains.IsRightHanded) {
				
		banner = new BannerView (adUnitId, AdSize.Banner, AdPosition.BottomLeft);
				} else {
		banner = new BannerView (adUnitId, AdSize.Banner, AdPosition.BottomRight);
				}

			}
		}

		if (banner != null) {

			// Called when an ad request has successfully loaded.
			banner.OnAdLoaded += Banner_OnAdLoaded;
			// Called when an ad request failed to load.
			banner.OnAdFailedToLoad += Banner_OnAdFailedToLoad;

			// Called when the user returned from the app after an ad click.
			banner.OnAdClosed += Banner_OnAdClosed;

			// Create an empty ad request.
			AdRequest request = new AdRequest.Builder ().Build ();
			// Load the banner with the request.
			banner.LoadAd (request);
		}
	}
	[HideInInspector]
	public bool IsAdLoading = false;
	
    void Banner_OnAdClosed (object sender, System.EventArgs e)
	{
		IsAdLoading = false;
	}

    void Banner_OnAdFailedToLoad (object sender, AdFailedToLoadEventArgs e)
    {
		IsAdLoading = false;
    }

    void Banner_OnAdLoaded (object sender, System.EventArgs e)
    {
		IsAdLoading = true;
    }

		/// <summary>
		/// Requests the interstitial.
		/// </summary>
    private void RequestInterstitial()
	{
		if (Contains.IsHavingRemoveAd)
		return;

#if UNITY_ANDROID
		string adUnitId = InterstitialAndroidAdUnityID;
#elif UNITY_IOS
		string adUnitId = InterstitialIOSAdUnityID;
#else
        string adUnitId = "unexpected_platform";
#endif

		// Initialize an InterstitialAd.
		interstitialAd = new InterstitialAd (adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ().Build ();
		// Load the interstitial with the request.
		interstitialAd.LoadAd (request);
	}

    #endregion

}
