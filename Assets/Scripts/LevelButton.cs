﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LevelButton : MonoBehaviour {

	[SerializeField] Image buttonImage;
	[SerializeField] Sprite buttonOn;
	[SerializeField] Sprite buttonOff;
	[SerializeField] Sprite buttonNew;
 	[SerializeField] Text number;
 	public int buttonIndex;
	public bool isUnlock = false;
	LevelSelection levelSelect;

	[SerializeField] GameObject[] stars;
	// Use this for initialization
	void Start () {


	}

	public void Init()
	{
		number.text = buttonIndex.ToString();
		levelSelect = GameObject.FindObjectOfType<LevelSelection>();

		if (buttonIndex < LevelLoader.Instance.latestLevel) SetUnlock();
		//if (buttonIndex <= 105) SetUnlock();
		else SetLock();
		if (buttonIndex == LevelLoader.Instance.latestLevel) SetButtonNew();
	}

	public void SetStar(int number)
	{
		for (int i = 0; i < 3; i++)
		{
			stars[i].SetActive(false);
		}

		for (int i = 0; i < number; i++)
		{
			stars[i].SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnButtonSelect()
	{
		if (isUnlock)
			levelSelect.LoadToInGame(buttonIndex);
	}

	public void SetLock()
	{
		buttonImage.sprite = buttonOff;
		isUnlock = false;

		for (int i = 0; i < 3; i++)
		{
			stars[i].transform.parent.gameObject.SetActive(false);
		}
	}

	public void SetUnlock()
	{
		buttonImage.sprite = buttonOn;
		isUnlock = true;
	}

	public void SetButtonNew()
	{
		buttonImage.sprite = buttonOn;
		isUnlock = true;

		Vector3 scale = transform.lossyScale + new Vector3(0.2f, 0.2f, 0.2f);
		transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
	}
}
