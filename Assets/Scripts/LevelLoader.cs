﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using MiniJSON;
using System.Collections.Generic;
using System.Linq;
using System;

public class LevelLoader : Singleton<LevelLoader> {

	public List<string> cardsDefine;
	string latestLevelString = "LATEST_LEVEL";
	public int latestLevel = 1;
	public int currentLevel = 1;
	public Enums.DifficultLevel difficultLevel = Enums.DifficultLevel.Easy;
	public bool visitHome = true;

	public enum GameMode {
		NONE = 0,
		ARCADE,
		CLASSIC
	}

	public GameMode currentMode = GameMode.CLASSIC;


	// Use this for initialization
	void Start () {
		//PlayerPrefs.SetInt(latestLevelString, 57);
		if (!PlayerPrefs.HasKey(latestLevelString)) PlayerPrefs.SetInt(latestLevelString, 1);
		latestLevel = PlayerPrefs.GetInt(latestLevelString);
		currentLevel = latestLevel;
	}

	public bool IsClassicMode()
	{
		return currentMode == GameMode.CLASSIC;
	}

	public void LoadCurrentlevel()
	{
		LoadLevel(currentLevel);
	}

	public void LoadLevel(int level)
	{
		currentMode = GameMode.ARCADE;
		currentLevel = level;
		cardsDefine.Clear();

		TextAsset jsonString;
		jsonString = Resources.Load("Levels/" + level.ToString("D3"), typeof(TextAsset)) as TextAsset;

		if (jsonString == null)
		{
			print("Can not load level data ");
			return;
		}

		var dict = Json.Deserialize(jsonString.text) as Dictionary<string, object>;
		var data = (List<object>)dict["data"];
		foreach (object datum in data)
		{
			if (datum != "NON")
				cardsDefine.Add(datum.ToString());
		}

		string mode = dict["mode"].ToString();
		Debug.Log("Difficult mdoe = " + mode);
		if (level < ExtraConfig.Instance.EasyModeLevel)
		{
			difficultLevel = Enums.DifficultLevel.Easy;
		}
		else if (level < ExtraConfig.Instance.NormalModeLevel)
		{
			difficultLevel = Enums.DifficultLevel.Medium;
		}
		else
		{
			difficultLevel = Enums.DifficultLevel.Hard;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void NextLevel()
	{
		currentLevel++;
		if (currentLevel > latestLevel) 
		{
			latestLevel = currentLevel;
			PlayerPrefs.SetInt(latestLevelString, latestLevel);
		}
	}
}
