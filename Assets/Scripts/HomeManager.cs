﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour {
	Image loadingFade;
	float loadingTime = 0.3f;

	// Use this for initialization
	void Start () {
		LevelLoader.Instance.currentMode = LevelLoader.GameMode.NONE;
		LevelLoader.Instance.difficultLevel = Enums.DifficultLevel.None;
		LevelLoader.Instance.visitHome = true;

		AdSystem.Instance.HideBanner();
	}
	
	// Update is called once per frame
	void Update () {
		if (UnityEngine.Input.GetKeyDown (KeyCode.Escape)) {
			DialogSystem.Instance.ShowYesNo("Solitaire Kings", "Do you want to quit the game.", () =>
				{
					Application.Quit();
				});
		}
	}

	void Awake()
	{
		loadingFade = GameObject.FindGameObjectWithTag("LoadingFade").GetComponent<Image>();
		loadingFade.color = Color.black;
		loadingFade.DOFade(0, loadingTime);
	}

	public void OnButtonArcade()
	{
		loadingFade.DOFade(1, loadingTime);
		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("LevelScreen");
		});

	}

	public void OnButtonClassic()
	{
		LevelLoader.Instance.currentMode = LevelLoader.GameMode.CLASSIC;
		loadingFade.DOFade(1, loadingTime);
		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("PlayingScreen");
		});

	}

	public void OnButtonHelp()
	{
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
		DialogSystem.Instance.ShowDialogHelp();
	}

	public void OnButtonOption()
	{
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

		DialogSystem.Instance.ShowDialogOptions ();
	}

	public void OnButtonVideo()
	{
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);
		DialogSystem.Instance.ShowDialogReward("none");
	}

	public void OnButtonGoToPlayStore()
	{		
		//Application.OpenURL ("market://details?id="+package+"");
		//Application.OpenURL ("market://details?q=pname:com.vinpearl.jewelsdeluxe");
		Application.OpenURL ("market://details?id="+Application.identifier+"");
	}



	// <summary>
	/// More game
	/// </summary>
	public void OnButtonGoToMoreGame()
	{		
		//Application.OpenURL ("market://search?id=Vinpearl+Studio");
		//Application.OpenURL ("market://search?id="+developer+"");
		Application.OpenURL ("market://search?id="+Application.companyName+"");
	}
}
