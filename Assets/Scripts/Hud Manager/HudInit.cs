﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HudInit : MonoBehaviour {

	public Transform Default_Style;

	public Transform Candy_Style;
	public Transform Style1;
	public Transform Style2;

	void OnEnable()
	{
		UpdateTheme();

	}

	public void UpdateTheme()
	{
		if (Contains.CurrentStyle == Enums.Themes.Default) {
			HideAllContent();
			if (Default_Style)Default_Style.gameObject.SetActive (true);

		} else if (Contains.CurrentStyle == Enums.Themes.Candy) {
			HideAllContent();
			if (Candy_Style)Candy_Style.gameObject.SetActive (true);
		}
		else if (Contains.CurrentStyle == Enums.Themes.Style1) {
			HideAllContent();
			if (Style1)Style1.gameObject.SetActive (true);
		}
		else 
		{
			HideAllContent();
			if (Style2)Style2.gameObject.SetActive (true);
		}

		if (LevelLoader.Instance.currentMode == LevelLoader.GameMode.ARCADE)
		{
			HideAllContent();
			if (LevelLoader.Instance.difficultLevel == Enums.DifficultLevel.Easy)
				GameManager.Instance.DifficultLevel = Enums.DifficultLevel.Easy;
			else if (LevelLoader.Instance.difficultLevel == Enums.DifficultLevel.Medium)
				GameManager.Instance.DifficultLevel = Enums.DifficultLevel.Medium;
			else GameManager.Instance.DifficultLevel = Enums.DifficultLevel.Hard;
		} 
		else
		{	
			if (!LevelLoader.Instance.visitHome && SceneManager.GetActiveScene().name == "PlayingScreen") 
			{
				HideAllContent();
				GameManager.Instance.DifficultLevel = LevelLoader.Instance.difficultLevel;
			}
		}
	}

	void HideAllContent()
	{
		if (Default_Style)Default_Style.gameObject.SetActive (false);
		if (Candy_Style)Candy_Style.gameObject.SetActive (false);
		if (Style1) Style1.gameObject.SetActive(false);
		if (Style2) Style2.gameObject.SetActive(false);
	}
}
