﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

/// <summary>
/// User interface image contents.
/// </summary>
[System.Serializable]
public struct UIImageContents
{
	/// <summary>
	/// The image undo.
	/// </summary>
	public Image[] ImageUndo;

	/// <summary>
	/// The image hint.
	/// </summary>
	public Image[] ImageHint;

	/// <summary>
	/// The image restart.
	/// </summary>
	public Image[] ImageRestart;

	/// <summary>
	/// The image themes.
	/// </summary>
	public Image[] ImageThemes;

	/// <summary>
	/// The image result cards.
	/// </summary>
	public Image[] ImageResultCards;

	/// <summary>
	/// The reset cards.
	/// </summary>
	public Image[] ResetCards;

	/// <summary>
	/// The portrait background.
	/// </summary>
	public MeshRenderer[] PortraitBackground;

	/// <summary>
	/// The landscape background.
	/// </summary>
	public MeshRenderer[] LandscapeBackground;


}

[System.Serializable]
public struct UISpriteContents
{
	/// <summary>
	/// The style identifier.
	/// </summary>
	public Enums.Themes StyleId;

	/// <summary>
	/// The sprite undo.
	/// </summary>
	public Sprite spriteUndo;

	/// <summary>
	/// The sprite restart.
	/// </summary>
	public Sprite spriteRestart;

	/// <summary>
	/// The sprite hint.
	/// </summary>
	public Sprite spriteHint;

	/// <summary>
	/// The sprite theme.
	/// </summary>
	public Sprite spriteTheme;

	/// <summary>
	/// The sprite result cards.
	/// </summary>
	public Sprite spriteResultCards;

	/// <summary>
	/// The sprite reset cards.
	/// </summary>
	public Sprite spriteResetCards;

	/// <summary>
	/// The sprite portrait background.
	/// </summary>
	public Material spritePortraitBackground;

	/// <summary>
	/// The sprite landscape background.
	/// </summary>
	public Material spriteLandscapeBackground;
}

/// <summary>
/// Hud system.
/// </summary>
public class HudSystem : Singleton < HudSystem > {

	// ================================== References ============================ //

	#region UI

	[Header ("UI Player")]

	/// <summary>
	/// The user interface time display.
	/// </summary>
	[SerializeField]
	private Text[] UITimeDisplay;

	/// <summary>
	/// The user interface score display.
	/// </summary>
	[SerializeField]
	private Text[] UIScoreDisplay;

    /// <summary>
    /// The user interface move display.
    /// </summary>
    [SerializeField]
	private Text[] UIMoveDisplay;

	[Header ("Banner Game")]
	/// <summary>
	/// The user interface banner.
	/// </summary>
	[SerializeField]
	private Transform UIBanner;

	/// <summary>
	/// The user interface window.
	/// </summary>
	[SerializeField]
	private Transform UIWin;

	/// <summary>
	/// The user interface lose.
	/// </summary>
	[SerializeField]
	private Transform UILose; 

	/// <summary>
	/// The hud UI.
	/// </summary>
	[SerializeField]
	private CanvasGroup HudUI;

    [Header("Hint")]
    [SerializeField]
    private Text UITextHint;

	/// <summary>
	/// The bar menu.
	/// </summary>
    [SerializeField]
    private RectTransform[] barMenu;

	[SerializeField] Transform[] autoWin;

	[Header ("Themes")]
	/// <summary>
	/// The image contents.
	/// </summary>
	public UIImageContents imageContents;

	/// <summary>
	/// The sprite contents.
	/// </summary>
	public UISpriteContents[] spriteContents;
		
	[SerializeField] GameObject star1;
	[SerializeField] GameObject star2;
	[SerializeField] GameObject star3;

	public bool isEnableBar = false;
    #endregion



    #region Variables

	/// <summary>
	/// The is ready restart.
	/// </summary>
    protected bool IsReadyRestart;
	#endregion
	Image loadingFade;
	float loadingTime = 0.3f;

	Tweener shuffleAnim = null;
	Tweener hintAnim = null;
	bool isPlayShuffleHightlight = false;
	#region Functional

	/// <summary>
	/// Updates the sprite.
	/// </summary>
	/// <param name="style">Style.</param>
	public void UpdateSprite(Enums.Themes style)
	{
		for (int i = 0; i < spriteContents.Length; i++) {

			// Check id style as same as the id from condition.
			if (spriteContents [i].StyleId == style) {
			
				// Update the sprite of Hint user interface.
				for (int j = 0; j < imageContents.ImageHint.Length; j++) {

					// Update the parameters from ImageHints.
					imageContents.ImageHint[j].sprite = spriteContents [i].spriteHint;
				}

				// Update the sprite of restart user interface.
				for (int j = 0; j < imageContents.ImageRestart.Length; j++) {

					// Update the parameters from ImageRestart.
					imageContents.ImageRestart[j].sprite = spriteContents [i].spriteRestart;
				}

				// Update the sprite of undo user interface.
				for (int j = 0; j < imageContents.ImageUndo.Length; j++) {

					// Update the parameters from ImageUndo.
					imageContents.ImageUndo[j].sprite = spriteContents [i].spriteUndo;
				}

				// Update the sprite of Result user interface.
				for (int j = 0; j < imageContents.ImageResultCards.Length; j++) {

					// Update the parameters from ImageResultCards.
					imageContents.ImageResultCards[j].sprite = spriteContents [i].spriteResultCards;
				}

				// Update the sprite of Themes user interface.
				for (int j = 0; j < imageContents.ImageThemes.Length; j++) {

					// Update the parameters from ImageThemes.
					imageContents.ImageThemes[j].sprite = spriteContents [i].spriteTheme;
				}

				// Update the sprite of Portrait Background.
				for (int j = 0; j < imageContents.PortraitBackground.Length; j++) {

					// Update the parameters from PortraitBackground.
					imageContents.PortraitBackground[j].material = spriteContents [i].spritePortraitBackground;
				}

				// Update the sprite of Landscape Background.
				for (int j = 0; j < imageContents.LandscapeBackground.Length; j++) {

					// Update the parameters from LandscapeBackground.
					imageContents.LandscapeBackground[j].material = spriteContents [i].spriteLandscapeBackground;
				}

				// Update the sprite of Reset Cards.
				for (int j = 0; j < imageContents.ResetCards.Length; j++) {

					// Update the parameters from ResetCards.
					imageContents.ResetCards[j].sprite = spriteContents [i].spriteResetCards;
				}

				break;
			}
		}
	}

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Start()
	{		 
		

		// Invoke time display when playing.
		InvokeRepeating ("UpdateTimeDisplay", 0 , 1 );

		// Reset number of move to zero.
		UpdateMove (0);

		// Reset number of score to zero.
		UpdateScore (0);

		// Hide the ads.
		DisableAds ();

		InvokeRepeating ("InvokeCalleAds", 0, 0.5f);

		loadingFade = GameObject.FindGameObjectWithTag("LoadingFade").GetComponent<Image>();
		loadingFade.color = Color.black;
		loadingFade.DOFade(0, loadingTime);

		for ( int i = 0 ; i <  autoWin.Length ; i++ )
		{
			autoWin[i].DOScale(new Vector3(1.15f, 1.15f, 1.15f), 0.6f).SetLoops(-1, LoopType.Yoyo);
		}
    }

	protected bool IsCalled = false;

	protected bool IsDisable = false;

	/// <summary>
	/// Invokes the calle ads.
	/// </summary>
	public void InvokeCalleAds()
	{

		if (!MutilResolution.Instance.IsPortrait) {
			return;
		}

		if (AdSystem.Instance.IsAdLoading && Contains.IsHavingRemoveAd == false && IsCalled == false ) {
			
			EnableBar ();	

			IsDisable = false;

			IsCalled = true;

		} else if (AdSystem.Instance.IsAdLoading == false && IsDisable == false || Contains.IsHavingRemoveAd  == true && IsDisable == false) {
			
			DisableBar ();

			IsCalled = false;

			IsDisable = true;

		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		// Cancel all invoke have used in this gameobject.
		CancelInvoke ();
	}

	#endregion

	#region Update
	/// <summary>
	/// Disables the ads.
	/// </summary>
	public void DisableAds()
	{
		// Check the condition if this game has removed ads.
		if (Contains.IsHavingRemoveAd) {

			// Hide Banner ads if it already turned on.
			AdSystem.Instance.HideBanner ();

			// Hide Full-screen ads if it already turned on.
			//AdSystem.Instance.HideInterstitialAd ();
		}
	}

	/// <summary>
	/// Enables the bar.
	/// </summary>
    public void EnableBar()
    {
		isEnableBar = true;
		if (MutilResolution.Instance.IsPortrait)
		for ( int i = 0 ; i <  autoWin.Length ; i++ )
		{
			autoWin[i].DOLocalMoveY(450f, Contains.DurationFade);
		}

		for ( int i = 0 ; i < barMenu.Length ; i++ )
		{
			print (barMenu [i].localPosition.y);

			// Kill all session using Dotween from barMenu.
			barMenu[i].DOKill ();

			// Moving the barmenu to start position.
			barMenu[i].DOLocalMoveY (200f, Contains.DurationFade);
		}
    }

	public void DisableBar()
	{
		isEnableBar = false;
		if (MutilResolution.Instance.IsPortrait)
		{
			for ( int i = 0 ; i <  autoWin.Length ; i++ )
			{
				autoWin[i].DOLocalMoveY(250f, Contains.DurationFade);
			}
		}

		for ( int i = 0 ; i < barMenu.Length ; i++ )
		{
			// Kill all session using Dotween from barMenu.
			barMenu[i].DOKill ();

			// Moving the barmenu to start position.
			barMenu[i].DOLocalMoveY (0, Contains.DurationFade);
		}
	}


	public void ShowAutoWin(bool value)
	{
		for ( int i = 0 ; i <  autoWin.Length ; i++ )
		{
			autoWin[i].gameObject.SetActive(value);
		}
	}

	/// <summary>
	/// Updates the time display.
	/// </summary>
	protected void UpdateTimeDisplay()
	{
		for (int i = 0; i < UITimeDisplay.Length; i++) {

			// Update time of game with UI.
			UITimeDisplay[i].text = TimeSystem.Instance.GetTimeDisplay ();
		}
	}

	/// <summary>
	/// Updates the score.
	/// </summary>
	public void UpdateScore(int param)
	{
		for (int i = 0; i < UIScoreDisplay.Length; i++) {
			
			// Update score of game with UI.
			UIScoreDisplay[i].text = string.Format ("Score: {0}", param);
		}
	}

	/// <summary>
	/// Updates the move.
	/// </summary>
	/// <param name="param">Parameter.</param>
	public void UpdateMove(int param)
	{
		for (int i = 0; i < UIMoveDisplay.Length; i++) {
			// Update move of game with UI.
			UIMoveDisplay[i].text = string.Format ("Moves: {0}", param);
		}
	}
		
	/// <summary>
	/// Enables the lose.
	/// </summary>
	public void EnableLose()
	{
		
		UIBanner.gameObject.SetActive (true);

		// Disable Banner Winning of game.
		UIWin.gameObject.SetActive (false);

		// Enable Banner losing of game.
		UILose.gameObject.SetActive (true);

		// Hide all UI of game.
		HudUI.DOFade (0, Contains.DurationFade);

		// Enable effect blur.
		GamePlay.Instance.EnableBlur ();

		// Playing music losing.
		SoundSystems.Instance.PlayerMusic ( Enums.MusicIndex.LoseMusic , false);

		// show info
		GameObject.Find("ScoreText").GetComponent<Text>().text = "Score : " + Contains.Score;
		GameObject.Find("TimeText").GetComponent<Text>().text = TimeSystem.Instance.GetTimeDisplay ();
		GameObject.Find("MoveText").GetComponent<Text>().text = "Move : " + Contains.Moves;

		// Running the function restart after 4s.
		Invoke ("ReadyRestart", 4f);

		// Showing the full-screen ads after 2s. 
        Invoke("InvokeShowAd", 2f);
    }

	/// <summary>
	/// Invokes the show ad.
	/// </summary>
    void InvokeShowAd()
    {
		// Showing the full-screen ads.
		AdSystem.Instance.ShowInterstitialAd();
    }

	/// <summary>
	/// Disables the window.
	/// </summary>
	public void EnableWin()
	{
		UIBanner.gameObject.SetActive (true);

		// Enable Winning banner.
		UIWin.gameObject.SetActive (true);

		// Disable Losing banner.
		UILose.gameObject.SetActive (false);

		// Disable hud of game.
		HudUI.gameObject.SetActive (false);

		// Fade Hud.
		HudUI.DOFade (0, Contains.DurationFade);

		bool showStar2 = false;
		bool showStar3 = false;
		int numStar = 1;
		int second = TimeSystem.Instance.Second + TimeSystem.Instance.Minute * 60;
		if (second < ExtraConfig.Instance.MinTimeToGetShuffle && TimeSystem.Instance.Hour <= 0) 
		{
			SupportManager.Instance.RewardShuffle(1, false);
			GameObject.Find("AddShuffleText").GetComponent<Text>().text = "+1 Shuffle";
			showStar2 = true;
		}
		else
		{
			GameObject.Find("AddShuffleText").SetActive(false);
		}
		if (Contains.Moves < ExtraConfig.Instance.MinMoveToGetHint) 
		{
			SupportManager.Instance.RewardHint(1, false);
			GameObject.Find("AddHintText").GetComponent<Text>().text = "+1 Hint";
			if (showStar2) showStar3 = true;
			else showStar2 = true;
		}
		else
		{
			GameObject.Find("AddHintText").SetActive(false);
		}

		if (showStar2) 
		{
			star2.SetActive(true);
			numStar = 2;

		}
		if (showStar3) 
		{
			star3.SetActive(true);
			numStar = 3;
		}

		// Update star
		Debug.Log("Is classic = " + LevelLoader.Instance.IsClassicMode() + " - " + LevelLoader.Instance.gameObject.name);
		if (!LevelLoader.Instance.IsClassicMode())
		{
			int currentStar = PlayerPrefs.GetInt("Level" + LevelLoader.Instance.currentLevel);
			if (numStar > currentStar) 
			{
				PlayerPrefs.SetInt("Level" + LevelLoader.Instance.currentLevel, numStar);
				Debug.Log("Star = " + currentStar + " - " + numStar);
			}

		}

		if (!LevelLoader.Instance.IsClassicMode())
		{
			Debug.Log("Is classic = " + LevelLoader.Instance.IsClassicMode() + " - " + LevelLoader.Instance.gameObject.name);
			LevelLoader.Instance.NextLevel();
		}

		// Enable Blur.
		GamePlay.Instance.EnableBlur ();

		// Playing winning sound.
		SoundSystems.Instance.PlayerMusic ( Enums.MusicIndex.WinMusic , false);

		// 
		SupportManager.Instance.RewardUndo(1, false	);
		GameObject.Find("AddUndoText").GetComponent<Text>().text = "+1 Undo";
		//Debug.Log("time = " + TimeSystem.Instance.Minute + " - " + TimeSystem.Instance.Hour);
		//Debug.Log("move = " + Contains.Moves);



		// show info
		GameObject.Find("ScoreText").GetComponent<Text>().text = "Score : " + Contains.Score;
		GameObject.Find("TimeText").GetComponent<Text>().text = TimeSystem.Instance.GetTimeDisplay ();
		GameObject.Find("MoveText").GetComponent<Text>().text = "Move : " + Contains.Moves;

		// Enable touch restart after 4s.
		Invoke ("ReadyRestart", 4f);

		// Enable Full-screen ads after 2s.
        Invoke("InvokeShowAd", 2f);
    }

	/// <summary>
	/// Readies the restart.
	/// </summary>
	void ReadyRestart()
	{
		IsReadyRestart = true;
	}

	/// <summary>
	/// Restarts the game.
	/// </summary>
	public void RestartGame()
	{
		// Touch to restart the game.
		if (IsReadyRestart) {
			if (LevelLoader.Instance.IsClassicMode())
			TouchEventSystem.Instance.DoRestartGame ();
			else TouchEventSystem.Instance.GoSelectLevel();
		}
	}

	public void OnButtonReplay()
	{
		GamePlay.Instance.StopAllCoroutine();

		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("PlayingScreen");
		});
	}

	public void OnButtonNext()
	{
		GamePlay.Instance.StopAllCoroutine();
		if (!LevelLoader.Instance.IsClassicMode())
		LevelLoader.Instance.LoadCurrentlevel();

		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("PlayingScreen");
		});
	}

	public void OnButtonHome()
	{
		GamePlay.Instance.StopAllCoroutine();

		loadingFade.DOFade(1, loadingTime);
		if (LevelLoader.Instance.currentMode == LevelLoader.GameMode.ARCADE)
		{
			DOVirtual.DelayedCall(loadingTime, () => {
				SceneManager.LoadScene("LevelScreen");
			});
		}
		else
		{
			DOVirtual.DelayedCall(loadingTime, () => {
				SceneManager.LoadScene("MenuScreen");
			});
		}

	}
	#endregion

	public void PlayHightlightHint()
	{
		if (isPlayShuffleHightlight) return;
		GameObject uiHint = GameObject.Find("UIHints");
		if (uiHint != null)
		{
			GameObject.Find("UIHints").transform.localScale = Vector3.one;
			if (hintAnim == null)
				hintAnim = GameObject.Find("UIHints").transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.6f).SetLoops(-1, LoopType.Yoyo);
		}
	}

	public void PlayHightlightShuffle()
	{
		isPlayShuffleHightlight = true;
		GameObject.Find("UIShuffle").transform.localScale = Vector3.one;
		if (shuffleAnim == null)
		shuffleAnim = GameObject.Find("UIShuffle").transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.6f).SetLoops(-1, LoopType.Yoyo);
	}

	public void StopHightlightHint()
	{
		//Debug.Log("Stop hint hightlight");

		if (hintAnim != null) hintAnim.Kill(true);
		hintAnim = null;

		GameObject uiHint = GameObject.Find("UIHints");
		if (uiHint != null)
			uiHint.transform.localScale = Vector3.one;
	}

	public void CheckShuffleHightlight()
	{
		if (isPlayShuffleHightlight)
		{
			if (shuffleAnim != null) shuffleAnim.Kill(true);
			shuffleAnim = null;
			PlayHightlightShuffle();
		}
	}

	public void StopHightlightShuffle()
	{
		isPlayShuffleHightlight = false;
		if (shuffleAnim != null) shuffleAnim.Kill(true);
		shuffleAnim = null;

		GameObject uiShuffle = GameObject.Find("UIShuffle");
		if (uiShuffle != null)
			uiShuffle.transform.localScale = Vector3.one;
	}


}
