﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Time system.
/// </summary>
public class TimeSystem : Singleton < TimeSystem > {

	// =============================== Variables ========================= //

	/// <summary>
	/// The hour.
	/// </summary>
	public int Hour;

	/// <summary>
	/// The minute.
	/// </summary>
	public int Minute;

	/// <summary>
	/// The second.
	/// </summary>
	public int Second;



	// ========================= Functional ======================= //

	/// <summary>
	/// Awake this instance.
	/// </summary>
	protected override void Awake ()
	{
		base.Awake ();

		InvokeRepeating ("UpdateTime", 0 , 1);
	}

	/// <summary>
	/// Updates the time.
	/// </summary>
	protected void UpdateTime()
	{
		if (GameManager.Instance.IsGameEnd () || !GameManager.Instance.IsGameReady() || !GameManager.Instance.isHaveAction) {
			return;
		}

		Second = Mathf.Clamp (Second + 1, 0, 60);

		Minute = Mathf.Clamp (Second - 60 >= 0 ? Minute + 1 : Minute, 0, 60);

		Hour = Mathf.Clamp (Minute - 60 >= 0 ? Hour + 1 : Hour, 0, 999);

		if (Second >= 60) {
			Second = 0;
		}

		if (Minute >= 60) {
			Minute = 0;
		}
	}

	/// <summary>
	/// Gets the time display.
	/// </summary>
	/// <returns>The time display.</returns>
	public string GetTimeDisplay()
	{
		return Hour == 0 ? string.Format ("Time: {0}:{1}", Minute.ToString("00") , Second.ToString("00") ) : string.Format ("Time: {0}:{1}:{2}", Hour.ToString("00") ,Minute.ToString("00") , Second.ToString("00") );
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		CancelInvoke ();
	}
}
