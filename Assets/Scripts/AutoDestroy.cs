﻿using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
	[SerializeField]
	float timeDestroy = 1;

	void Update ()
	{
		timeDestroy -= Time.deltaTime;
		if (timeDestroy <= 0)
			Destroy(gameObject);
	}
}
