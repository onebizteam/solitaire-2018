﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class DialogOptions : DialogInterface {

	[Header ("Contents")]
	/// <summary>
	/// The content default.
	/// </summary>
	public Transform Content_Default;

	/// <summary>
	/// The content candy.
	/// </summary>
	public Transform Content_Candy;
	public Transform Content_Style1;
	public Transform Content_Style2;

	Image loadingFade;
	float loadingTime = 0.3f;

	[Header ("Toggle")]
	/// <summary>
	/// The handle.
	/// </summary>
	public GameObject[] LeftHandle;

	public GameObject[] RightHandle;

	/// <summary>
	/// The sound.
	/// </summary>
	public Toggle[] Sound;

	/// <summary>
	/// The music.
	/// </summary>
	public Toggle[] Music;

	/// <summary>
	/// The remove ads.
	/// </summary>
	public Button[] RemoveAds;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
//		for (int i = 0; i < Handle.Length; i++) {
//			Handle [i].onValueChanged.AddListener ((bool arg0) => {
//				
//				if (arg0 == true) {
//					Contains.IsRightHanded = true;
//
//				} else {
//					Contains.IsRightHanded = false;
//				}		
//
//				if (GamePlay.Instance != null && MutilResolution.Instance != null) {
//					if (MutilResolution.Instance.IsPortrait) {
//						GamePlay.Instance.UpdatePortrait ();	
//					} else {
//						GamePlay.Instance.UpdateLandscape ();
//					}
//				}
//			});
//		}

		for (int i = 0; i < Sound.Length; i++) {

			if (Sound != null) {
				Sound[i].onValueChanged.AddListener ((bool arg0) => {
					if (arg0 == true) {

						Contains.IsSoundOn = true;

						SoundSystems.Instance.EnableSound ();
					} else {
						Contains.IsSoundOn = false;

						SoundSystems.Instance.DisableSound ();
					}			

				});
			}
		}

		for (int j = 0; j < Music.Length; j++) {

			if (Music != null) {
				Music[j].onValueChanged.AddListener ((bool arg0) => {
		
					if (arg0 == true) {

						Contains.IsMusicOn = true;

						SoundSystems.Instance.EnableMusic ();
					} else {

						Contains.IsMusicOn = false;

						SoundSystems.Instance.DisableMusic ();
					}
			
				});
			}
		}
	}

	public void OnButtonLeftHandle()
	{
		Contains.IsRightHanded = false;
		UpdateHandleLeftRight();
	}

	public void OnButtoRightHandle()
	{
		Contains.IsRightHanded = true;
		UpdateHandleLeftRight();
	}

	public void UpdateHandleLeftRight()
	{
		if (Contains.IsRightHanded)
		{
			for (int i = 0; i < LeftHandle.Length; i++) {
				LeftHandle[i].SetActive(false);
				RightHandle[i].SetActive(true);
			}
		}
		else
		{
			for (int i = 0; i < LeftHandle.Length; i++) {
				LeftHandle[i].SetActive(true);
				RightHandle[i].SetActive(false);
			}
		}

		if (GamePlay.Instance != null && MutilResolution.Instance != null) {
			if (MutilResolution.Instance.IsPortrait) {
				GamePlay.Instance.UpdatePortrait ();	
			} else {
				GamePlay.Instance.UpdateLandscape ();
			}
		}
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
//		for (int i = 0; i < Handle.Length; i++) {
//
//			if (Contains.IsRightHanded) {
//				if (Handle != null) {
//					Handle[i].isOn = true;
//				}
//			} else {
//				if (Handle != null) {
//					Handle[i].isOn = false;
//				}
//			}
//		}
		UpdateHandleLeftRight();
		for (int i = 0; i < RemoveAds.Length; i++) {

			if (Contains.IsHavingRemoveAd) {
				RemoveAds[i].interactable = false;
			}
		}

		for (int i = 0; i < Sound.Length; i++) {
			if (Contains.IsSoundOn) {
				Sound [i].isOn = true;
			}else{
				Sound [i].isOn = false;
			}
		}

		for (int i = 0; i < Music.Length; i++) {
			if (Contains.IsMusicOn) {
				Music [i].isOn = true;
			}else{
				Music [i].isOn = false;
			}
		}
	}

	/// <summary>
	/// Show this instance.
	/// </summary>
	public override void Show ()
	{
		if (Contains.CurrentStyle == Enums.Themes.Default) {
			HideAllContent();
			Content_Default.gameObject.SetActive (true);

			Content_Default.SetAsFirstSibling ();
		} else if (Contains.CurrentStyle == Enums.Themes.Candy){
			HideAllContent();
			Content_Candy.gameObject.SetActive (true);
			Content_Candy.SetAsFirstSibling ();
		}
		else if (Contains.CurrentStyle == Enums.Themes.Style1){
			HideAllContent();
			Content_Style1.gameObject.SetActive (true);
			Content_Style1.SetAsFirstSibling ();
		}
		else {
			HideAllContent();
			Content_Style2.gameObject.SetActive (true);
			Content_Style2.SetAsFirstSibling ();
		}

		base.Show ();	
	}

	public void OnButtonHome()
	{
		Hide();
		AdSystem.Instance.ShowInterstitialAd();
		GamePlay.Instance.StopAllCoroutine();
		loadingFade.DOFade(1, loadingTime);
		DOVirtual.DelayedCall(loadingTime, () => {
			SceneManager.LoadScene("MenuScreen");
		});
	}

	void HideAllContent()
	{
		Content_Default.gameObject.SetActive (false);
		Content_Candy.gameObject.SetActive (false);
		Content_Style1.gameObject.SetActive(false);
		Content_Style2.gameObject.SetActive(false);
	}

	/// <summary>
	/// Shows the themes dialog.
	/// </summary>
	public void ShowThemesDialog()
	{
		DialogSystem.Instance.ShowDialogThemes ();
	}

	public void OnButtonWatchVideo()
	{
		DialogSystem.Instance.ShowDialogReward("none");
	}

	/// <summary>
	/// Shows the remove ads.
	/// </summary>
	public void ShowRemoveAds(){
		
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

		DialogSystem.Instance.ShowYesNo ("Confirmation", string.Format ("Do you want to remove the ads with {0}", IapManager.Instance.ReturnThePrice ()), () => {
			IapManager.Instance.BuyNonConsumable();
		}	
		);
	}

	public override void Close (System.Action OnClose)
	{
		Hide ();

		base.Close (OnClose);
	}
}
