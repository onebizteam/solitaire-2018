﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogYesNo : DialogInterface {

    [Header("UI")]
    [SerializeField]
    private Text[] UITitle;

    [SerializeField]
    private Text[] UIMessage;

	[Header ("Contents")]
	public Transform Content_Default;
	public Transform Content_Candy;
	public Transform Content_Style1;
	public Transform Content_Style2;

    // ======================== Variables ====================== //

    protected string title;

    protected string message;

    // ============================ Action ====================== //
    protected System.Action OnClose;

    protected System.Action OnYes;

    protected System.Action OnNo;

    public void Init(string title, string message, System.Action OnYes = null, System.Action OnNo = null, System.Action OnClose = null)
    {


        this.title = title;

        this.message = message;

        this.OnNo = OnNo;

        this.OnYes = OnYes;

        this.OnClose = OnClose;
    }

    public override void Show()
    {
		if (Contains.CurrentStyle == Enums.Themes.Default) {
			HideAllContent();
			Content_Default.gameObject.SetActive (true);

			Content_Default.SetAsFirstSibling ();
		} else if (Contains.CurrentStyle == Enums.Themes.Candy) {
			HideAllContent();
			Content_Candy.gameObject.SetActive (true);
			Content_Candy.SetAsFirstSibling ();
		}
		else if (Contains.CurrentStyle == Enums.Themes.Style1) {
			HideAllContent();
			Content_Style1.gameObject.SetActive (true);
			Content_Style1.SetAsFirstSibling ();
		}
		else  {
			HideAllContent();
			Content_Style2.gameObject.SetActive (true);
			Content_Style2.SetAsFirstSibling ();
		}

		for (int i = 0; i < UITitle.Length; i++) {


			UITitle[i].text = title;
		}

		for (int i = 0; i < UIMessage.Length; i++) {


			UIMessage[i].text = message;
		}
        base.Show();  
    }

	void HideAllContent()
	{
		Content_Default.gameObject.SetActive (false);
		Content_Candy.gameObject.SetActive (false);
		Content_Style1.gameObject.SetActive(false);
		Content_Style2.gameObject.SetActive(false);
	}

    public override void Close(Action OnClose = null)
    {
        Hide();  

        base.Close(OnClose);

        if (this.OnClose != null)
        {
            this.OnClose();
        }
    }

    public override void Close()
    {
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

        Hide();

        base.Close();

        if (this.OnClose != null)
        {
            this.OnClose();
        }
    }

    public void No()
    {
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

        Hide();

        if (OnNo != null)
        {
            OnNo();
        }       
    }

    public void Yes()
    {
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

        Hide();

        if ( OnYes  != null)
        {
            OnYes();
        }     
    }
}
