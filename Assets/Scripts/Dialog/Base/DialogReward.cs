﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class DialogReward : DialogInterface {

	[Header ("Contents")]
	/// <summary>
	/// The content default.
	/// </summary>
	public Transform Content_Default;
	public Transform Content_Candy;
	public Transform Content_Style1;
	public Transform Content_Style2;

	Image loadingFade;
	float loadingTime = 0.3f;

	Tweener hintAnim = null;
	Tweener undoAnim = null;
	Tweener shuffleAnim = null;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
	}

	/// <summary>
	/// Show this instance.
	/// </summary>
	public override void Show ()
	{
		{
			if (Contains.CurrentStyle == Enums.Themes.Default) {
				HideAllContent();
				Content_Default.gameObject.SetActive (true);

				Content_Default.SetAsFirstSibling ();
			} else if (Contains.CurrentStyle == Enums.Themes.Candy){
				HideAllContent();
				Content_Candy.gameObject.SetActive (true);
				Content_Candy.SetAsFirstSibling ();
			}
			else if (Contains.CurrentStyle == Enums.Themes.Style1){
				HideAllContent();
				Content_Style1.gameObject.SetActive (true);
				Content_Style1.SetAsFirstSibling ();
			}
			else {
				HideAllContent();
				Content_Style2.gameObject.SetActive (true);
				Content_Style2.SetAsFirstSibling ();
			}
		}


		base.Show ();	
	}

	public void ShowReward(string rewardName)
	{
		Vector3 scale = new Vector3(1.2f, 1.2f, 1.2f);

		if (rewardName == "hint")
		{
			StopAllAnim();
			GameObject.Find("HintVideo").transform.Find("hightlight").gameObject.SetActive(true);
			GameObject.Find("ShuffleVideo").transform.Find("hightlight").gameObject.SetActive(false);
			GameObject.Find("UndoVideo").transform.Find("hightlight").gameObject.SetActive(false);

			hintAnim = GameObject.Find("HintVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
		} else if (rewardName == "undo")
		{
			StopAllAnim();
			GameObject.Find("HintVideo").transform.Find("hightlight").gameObject.SetActive(false);
			GameObject.Find("ShuffleVideo").transform.Find("hightlight").gameObject.SetActive(false);
			GameObject.Find("UndoVideo").transform.Find("hightlight").gameObject.SetActive(true);

			undoAnim = GameObject.Find("UndoVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
		} 
		else if (rewardName == "shuffle")
		{
			StopAllAnim();
			GameObject.Find("HintVideo").transform.Find("hightlight").gameObject.SetActive(false);
			GameObject.Find("ShuffleVideo").transform.Find("hightlight").gameObject.SetActive(true);
			GameObject.Find("UndoVideo").transform.Find("hightlight").gameObject.SetActive(false);
			shuffleAnim = GameObject.Find("ShuffleVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
		}
		else
		{
			StopAllAnim();

			GameObject.Find("HintVideo").transform.Find("hightlight").gameObject.SetActive(true);
			GameObject.Find("ShuffleVideo").transform.Find("hightlight").gameObject.SetActive(true);
			GameObject.Find("UndoVideo").transform.Find("hightlight").gameObject.SetActive(true);
			shuffleAnim = GameObject.Find("ShuffleVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
			undoAnim = GameObject.Find("UndoVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
			hintAnim = GameObject.Find("HintVideo").transform.DOScale(scale, 0.6f).SetLoops(-1, LoopType.Yoyo);
		}

		GameObject.Find("HintVideo").transform.Find("Text").GetComponent<Text>().text = "+" + ExtraConfig.Instance.HintPerReward;
		GameObject.Find("ShuffleVideo").transform.Find("Text").GetComponent<Text>().text = "+" + ExtraConfig.Instance.ShufflePerReward;
		GameObject.Find("UndoVideo").transform.Find("Text").GetComponent<Text>().text = "+" + ExtraConfig.Instance.UndoPerReward;
	}

	void StopAllAnim()
	{
		if (hintAnim != null) hintAnim.Kill(true);
		if (undoAnim != null) undoAnim.Kill(true);
		if (shuffleAnim != null) shuffleAnim.Kill(true);

		GameObject.Find("HintVideo").transform.localScale = Vector3.one;
		GameObject.Find("ShuffleVideo").transform.localScale = Vector3.one;
		GameObject.Find("UndoVideo").transform.localScale = Vector3.one;
	}

	public void HideAllHighlight()
	{
		GameObject.Find("HintVideo").transform.Find("hightlight").gameObject.SetActive(true);
	}

	void HideAllContent()
	{
		Content_Default.gameObject.SetActive (false);
		Content_Candy.gameObject.SetActive (false);
		Content_Style1.gameObject.SetActive(false);
		Content_Style2.gameObject.SetActive(false);
	}

	public void OnButtonFreeShuffle()
	{
		AdSystem.Instance.ShowAdmobReward((success) => {
			if (success)
			{
				SupportManager.Instance.RewardShuffle(ExtraConfig.Instance.ShufflePerReward);
				this.Hide();
			}});
	}

	public void OnButtonFreeUndo()
	{
		AdSystem.Instance.ShowAdmobReward((success) => {
			if (success) 
			{
				SupportManager.Instance.RewardUndo(ExtraConfig.Instance.UndoPerReward);
				this.Hide();
			}
		});
	}

	public void OnButtonFreeHint()
	{
		AdSystem.Instance.ShowAdmobReward((success) => {
			if (success) 
			{
				SupportManager.Instance.RewardHint(ExtraConfig.Instance.HintPerReward);
				this.Hide();
			}
		});
	}

	public override void Close (System.Action OnClose)
	{
		Hide ();

		base.Close (OnClose);
	}
}
