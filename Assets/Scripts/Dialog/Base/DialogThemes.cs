﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogThemes : DialogInterface {

	[Header ("Contents")]
	public Transform Content_Default;
	public Transform Content_Candy;
	public Transform Content_Style1;
	public Transform Content_Style2;

	public override void Show ()
	{
		if (Contains.CurrentStyle == Enums.Themes.Default) {
			HideAllContent();
			Content_Default.gameObject.SetActive (true);

			Content_Default.SetAsFirstSibling ();
		} else if (Contains.CurrentStyle == Enums.Themes.Candy) {
			HideAllContent();

			Content_Candy.gameObject.SetActive (true);

			Content_Candy.SetAsFirstSibling ();
		}
		else if (Contains.CurrentStyle == Enums.Themes.Style1) {
			HideAllContent();

			Content_Style1.gameObject.SetActive (true);

			Content_Style1.SetAsFirstSibling ();
		}
		else {
			HideAllContent();

			Content_Style2.gameObject.SetActive (true);

			Content_Style2.SetAsFirstSibling ();
		}

		base.Show ();
	}

	void HideAllContent()
	{
		Content_Default.gameObject.SetActive (false);
		Content_Candy.gameObject.SetActive (false);
		Content_Style1.gameObject.SetActive(false);
		Content_Style2.gameObject.SetActive(false);
	}

	/// <summary>
	/// Changes the theme.
	/// </summary>
	/// <param name="id">Identifier.</param>
	public void ChangeTheme(int id)
	{
		SoundSystems.Instance.PlaySound (Enums.SoundIndex.Press);

		if (SceneManager.GetActiveScene().name == "PlayingScreen")
		{
			switch (id) {
			case 0:

				GamePlay.Instance.ChangeStyleCards (Enums.Themes.Default);

				break;
			case 1:

				GamePlay.Instance.ChangeStyleCards (Enums.Themes.Candy);

				break;
			case 2:
				GamePlay.Instance.ChangeStyleCards (Enums.Themes.Style1);
				break;
			case 3:
				GamePlay.Instance.ChangeStyleCards (Enums.Themes.Style2);
				break;
			default:

				GamePlay.Instance.ChangeStyleCards (Enums.Themes.Default);

				break;
			}
		}
		else
		{
			Contains.CurrentStyle = (Enums.Themes)id;

			ThemeImage hud = GameObject.FindObjectOfType<ThemeImage>();
			if (hud != null) hud.UpdateTheme();
		}

		Hide ();

	}

	public override void Close (System.Action OnClose)
	{
		Hide ();

		base.Close (OnClose);
	}


}
