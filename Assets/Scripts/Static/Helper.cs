﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public static class Helper  {

	/// <summary>
	/// Gets the world position.
	/// </summary>
	/// <returns>The world position.</returns>
	public static Vector3 GetWorldPosition()
	{
		Vector3 valueReturn = Input.mousePosition;

		valueReturn = Camera.main.ScreenToWorldPoint (valueReturn);

		return valueReturn;
	}


	/// <summary>
	/// Sorts the cards.
	/// </summary>
	/// <param name="nubmerLastCard">Nubmer last card.</param>
	/// <param name="distance">Distance.</param>
	public static void SortCards(List < CardBehaviour > cardSort , Transform holder  ,int nubmerLastCard = 3, Enums.Direction direction = Enums.Direction.Right ,float distance = Contains.DistanceSortUnlockedCards , bool IsUseAnimation = false)
	{
		if (MutilResolution.Instance.IsPortrait) distance *= 0.6f;
		Vector3 PositionStart = holder.position;

		int litmitCards = Mathf.Clamp (cardSort.Count - nubmerLastCard,0, int.MaxValue);

		CardBehaviour card;

		for (int i = 0; i < litmitCards; i++) {

            card = cardSort[i];

            if (card.transform.parent != holder)
            {
                card.transform.SetParent(holder);

                card.transform.localScale = Vector3.one;
            }

            card.targetPositionCards = PositionStart;

            if (IsUseAnimation)
			{             
                card.MovingToPosition(card.targetPositionCards);
			}else
			{                
                card.transform.position = card.targetPositionCards;
			}
        }

		for (int i = litmitCards; i < cardSort.Count ; i++) {

			card = cardSort [i];

            if (card.transform.parent != holder)
            {
                card.transform.SetParent(holder);

                card.transform.localScale = Vector3.one;
            }

            card.targetPositionCards = PositionStart;

            if (IsUseAnimation)
            {             
                card.MovingToPosition(card.targetPositionCards);
            }else
            {
                card.transform.position = card.targetPositionCards;
            }

            switch ( direction)
            {
                case Enums.Direction.Down:


                    PositionStart.y = PositionStart.y - Math.Abs ( distance );

                    break;
                case Enums.Direction.Left:


                    PositionStart.x = PositionStart.x - Math.Abs(distance);

                    break;
                case Enums.Direction.Right:

                    PositionStart.x = PositionStart.x + Math.Abs(distance);

                    break;
                case Enums.Direction.Up:


                    PositionStart.y = PositionStart.y + Math.Abs(distance);

                    break;
            }
		}
	}

	public static void SwapCard(CardBehaviour card1, CardBehaviour card2)
	{
		
		//Debug.Log(card1.transform.GetSiblingIndex() + " - " + card2.transform.GetSiblingIndex());
		int sibling1 = card1.transform.GetSiblingIndex();
		int sibling2 = card2.transform.GetSiblingIndex();
		Vector3 tempPos1 = card1.transform.position;
		Vector3 tempPos2 = card2.transform.position;

		Transform tempParent = card1.transform.parent;
		card1.transform.parent = card2.transform.parent;
		card2.transform.parent = tempParent;

		card1.MovingToPosition(tempPos2, false, null, 0.5f);
		card2.MovingToPosition(tempPos1, false, null, 0.5f);

		Enums.CardBoard card1State = card1.GetCardState();
		Enums.CardBoard card2State = card2.GetCardState();

		card1.UpdateStateCard(card2State);
		card2.UpdateStateCard(card1State);


		card1.transform.SetSiblingIndex(sibling2);
		card2.transform.SetSiblingIndex(sibling1);

		DOVirtual.DelayedCall(0.5f, () => {
			card1.UpdatePosition();
			card2.UpdatePosition();
		});

		//
		//Debug.Log(card1.transform.GetSiblingIndex() + " - " + card2.transform.GetSiblingIndex());
	}

	/// <summary>
	/// Sorts the unlocked cards.
	/// </summary>
	/// <param name="cardSort">Card sort.</param>
	/// <param name="holder">Holder.</param>
	/// <param name="direction">Direction.</param>
	/// <param name="distanceUnlocked">Distance unlocked.</param>
	/// <param name="distanceLocked">Distance locked.</param>
	/// <param name="IsUseAnimation">If set to <c>true</c> is use animation.</param>
    public static void SortUnlockedCards(List<CardBehaviour> cardSort, Transform holder, Enums.Direction direction = Enums.Direction.Down, float distanceUnlocked = Contains.DistanceSortUnlockedCards, float distanceLocked = Contains.DistanceSortLockedCards,  bool IsUseAnimation = false)
    {
		if (MutilResolution.Instance.IsPortrait) distanceUnlocked *= 0.6f;
        Vector3 PositionStart = holder.position;

        CardBehaviour card;

        for (int i = 0; i < cardSort.Count; i++)
        {
            card = cardSort[i];

            if (card.transform.parent != holder)
            {
                card.transform.SetParent(holder);

                card.transform.localScale = Vector3.one;
            }

            card.targetPositionCards = PositionStart;

            if (IsUseAnimation)
            {
                card.MovingToPosition(card.targetPositionCards);
            }
            else
            {
                card.transform.position = card.targetPositionCards;
            }

            if (card.IsUnlocked())
            {
                switch (direction)
                {
                    case Enums.Direction.Down:

                        PositionStart.y = PositionStart.y - Math.Abs ( distanceUnlocked );
					Debug.Log("Sort unlock card: " + distanceUnlocked);
                        break;
                    case Enums.Direction.Left:

                        PositionStart.x = PositionStart.x - Math.Abs(distanceUnlocked);
                        break;
                    case Enums.Direction.Right:

                        PositionStart.x = PositionStart.x + Math.Abs(distanceUnlocked);
                        break;
                    case Enums.Direction.Up:

                        PositionStart.y = PositionStart.y + Math.Abs(distanceUnlocked);
                        break;
                }

            }else
            {
                switch (direction)
                {
                    case Enums.Direction.Down:
                        PositionStart.y = PositionStart.y - Math.Abs(distanceLocked);
                        break;
                    case Enums.Direction.Left:
                        PositionStart.x = PositionStart.x - Math.Abs(distanceLocked);
                        break;
                    case Enums.Direction.Right:
                        PositionStart.x = PositionStart.x + Math.Abs(distanceLocked);
                        break;
                    case Enums.Direction.Up:
                        PositionStart.y = PositionStart.y + Math.Abs(distanceLocked);
                        break;
                }            
            }
        }
    }

	/// <summary>
	/// Gets the position in the holder cards.
	/// </summary>
	/// <returns>The position in the holder cards.</returns>
	/// <param name="CardsHolder">Cards holder.</param>
	/// <param name="direction">Direction.</param>
	/// <param name="distanceUnlocked">Distance unlocked.</param>
	/// <param name="distanceLocked">Distance locked.</param>
    public static Vector3 GetPositionInTheHolderCards(List < CardBehaviour > CardsHolder, Enums.Direction direction, float distanceUnlocked = Contains.DistanceSortUnlockedCards, float distanceLocked = Contains.DistanceSortLockedCards)
    {
		if (MutilResolution.Instance.IsPortrait) distanceUnlocked *= 0.6f;
        Vector3 position = Vector3.zero;

        if (CardsHolder.Count > 0)
        {
            CardBehaviour card = CardsHolder[CardsHolder.Count - 1];

            position = card.targetPositionCards;

            switch (direction)
            {
                case Enums.Direction.Down:

                    if (CardsHolder[CardsHolder.Count - 1].IsUnlocked())
                    {
                        position.y = card.targetPositionCards.y - distanceUnlocked;
                    }else
                    {
                        position.y = card.targetPositionCards.y - distanceLocked;
                    }

                    break;
                case Enums.Direction.Left:

                    if (CardsHolder[CardsHolder.Count - 1].IsUnlocked())
                    {
                        position.x = card.targetPositionCards.x - distanceUnlocked;
                    }
                    else
                    {
                        position.x = card.targetPositionCards.x - distanceLocked;
                    }

                    break;
                case Enums.Direction.Right:

                    if (CardsHolder[CardsHolder.Count - 1].IsUnlocked())
                    {
                        position.x = card.targetPositionCards.x + distanceUnlocked;
                    }
                    else
                    {
                        position.x = card.targetPositionCards.x + distanceLocked;
                    }

                    break;
                case Enums.Direction.Up:

                    if (CardsHolder[CardsHolder.Count - 1].IsUnlocked())
                    {
                        position.y = card.targetPositionCards.y + distanceUnlocked;
                    }
                    else
                    {
                        position.y = card.targetPositionCards.y + distanceLocked;
                    }

                    break;
            }
        }

        return position;
    }

	/// <summary>
	/// Sorts the random.
	/// </summary>
	/// <returns>The random.</returns>
	/// <param name="cards">Cards.</param>
    public static List < CardBehaviour > SortRandom(List < CardBehaviour > cards)
    {
        List<CardBehaviour> cardsReturn = new List<CardBehaviour>();

        List<CardBehaviour> cardsGet = new List<CardBehaviour>(cards);

        CardBehaviour card;

        while (cardsGet.Count != 0)
        {
            card = cardsGet[UnityEngine.Random.Range(0, cardsGet.Count)];
            cardsGet.Remove(card);

            cardsReturn.Add(card);
        }

        return cardsReturn;
    }


	/// <summary>
	/// Sorts the random.
	/// </summary>
	/// <returns>The random.</returns>
	/// <param name="cards">Cards.</param>
	public static List < CardBehaviour > SortRandomWithDefine(List < CardBehaviour > cards)
	{
		List<CardBehaviour> cardsReturn = new List<CardBehaviour>();

		List<CardBehaviour> cardsGet = new List<CardBehaviour>(cards);

		CardBehaviour[] tempArray = new CardBehaviour[52];
		Enums.CardType type = Enums.CardType.None;
		Enums.CardVariables variable = Enums.CardVariables.Eight;
		string cardName = "";
		int trueIndex = 0;
		CardBehaviour card;

//		for (int i = 0; i < cardsGet.Count; i++)
//		{
//			Debug.Log(cardsGet[i].GetDataCard().GetCardSprite().name);
//		}

		for (int i = 0; i < 49; i++)
		{
			cardName = LevelLoader.Instance.cardsDefine[i];
			if (cardName != "RAN" && cardName != "NON")
			{
				if (cardName.Substring(0, 1) == "C") type = Enums.CardType.Club;
				if (cardName.Substring(0, 1) == "D") type = Enums.CardType.Diamond;
				if (cardName.Substring(0, 1) == "H") type = Enums.CardType.Heart;
				if (cardName.Substring(0, 1) == "S") type = Enums.CardType.Spade;
				int varConvert = int.Parse(cardName.Substring(1, 2));
				int cardIndex = 0;

				for (int j = 0; j < cardsGet.Count; j++)
				{
					if (cardsGet[j].GetDataCard().GetCardType() == type 
						&& cardsGet[j].GetDataCard().GetCardValue() == varConvert)
					{
						cardIndex = j;
						break;
					}
				}

				tempArray[trueIndex] = cardsGet[cardIndex];
				cardsGet.RemoveAt(cardIndex);
			}
			if (cardName != "NON") trueIndex++;
		}


		for (int i = 0; i < 52; i++)
		{
			if (tempArray[i] == null)
			{
				card = cardsGet[UnityEngine.Random.Range(0, cardsGet.Count)];
				cardsGet.Remove(card);
				tempArray[i] = card;
			}
		}

		for (int i = 0; i < 52; i++)
		{
			cardsReturn.Add(tempArray[i]);
		}

		return cardsReturn;
	}

	/// <summary>
	/// Updates the score.
	/// </summary>
	/// <param name="param">Parameter to add.</param>
	public static void UpdateScore(int param)
	{
		Contains.Score = Mathf.Clamp (Contains.Score + param, 0, 99999);
	}
}
