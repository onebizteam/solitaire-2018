﻿using UnityEngine;

/// <summary>
/// The class have any static global variables.
/// </summary>

public static class Contains
{

    /// <summary>
    /// The game play scene.
    /// </summary>
	public const string GamePlayScene = "MenuScreen";

    /// <summary>
    /// The distance sort unlocked cards.
    /// </summary>
    public const float DistanceSortUnlockedCards = 1.0f;

    /// <summary>
    /// The distance sort locked cards.
    /// </summary>
    public const float DistanceSortLockedCards = 0.3f;

    /// <summary>
    /// The distance sort review.
    /// </summary>
    public const float DistanceSortReview = 0.1f;

    /// <summary>
    /// The distance sort hint cards.
    /// </summary>
    public const float DistanceSortHintCards = 0.7f;

    /// <summary>
    /// The maximum holder cards.
    /// </summary>
    public const int MaximumHolderCards = 7;

    /// <summary>
    /// The off set width card.
    /// </summary>
    public const float OffSetWidthCard = 1.5f;

    /// <summary>
    /// The off set height card.
    /// </summary>
    public const float OffSetHeightCard = 2f;

    #region Animation

    /// <summary>
    /// The duration moving.
    /// </summary>
    public const float DurationMoving = 0.1f;

    /// /// <summary>
    /// The duration draw.
    /// </summary>
    public const float DurationDraw = 0.07f;

    /// <summary>
    /// The duration fade.
    /// </summary>
    public const float DurationFade = 0.4f;

    #endregion

    #region Wait Coroutine

    /// <summary>
    /// The duration preview.
    /// </summary>
    public const float DurationPreview = 1f;

    #endregion

    #region static
    /// <summary>
    /// The is sound on.
    /// </summary>
    public static bool IsSoundOn = true;

    /// <summary>
    /// The is music on.
    /// </summary>
    public static bool IsMusicOn = true;


    /// <summary>
    /// The moves.
    /// </summary>
    private static int moves = 0;

    /// <summary>
    /// Gets or sets the moves.
    /// </summary>
    /// <value>The moves.</value>
    public static int Moves
    {
        set
        {
            moves = Mathf.Clamp(value, 0, 99999);
        }

        get
        {
            return moves;
        }
    }
    #endregion
    /// <summary>
    /// The number cards.
    /// </summary>
    public const int NumberCards = 52;

	/// <summary>
	/// The is having remove ad.
	/// </summary>
	public static  bool IsHavingRemoveAd {
		get {

			return PlayerPrefs.GetInt ("IsHaveRemoveAd", 0) == 1;
		 
		}

		set {
			int param = value == true ? 1 : 0;

			PlayerPrefs.SetInt ("IsHaveRemoveAd", param);
		}
	}

	/// <summary>
	/// Gets or sets the current style.
	/// </summary>
	/// <value>The current style.</value>
	public static Enums.Themes CurrentStyle {
		get {
			if (PlayerPrefs.GetString("Style", "Default") == "Default") return Enums.Themes.Default;
			else if (PlayerPrefs.GetString("Style", "Default") == "Candy") return Enums.Themes.Candy;
			else if (PlayerPrefs.GetString("Style", "Default") == "Style1") return Enums.Themes.Style1;
			else return Enums.Themes.Style2;
		}

		set {
			
			Enums.Themes param = value;

			switch (param) {
			case Enums.Themes.Candy:

				PlayerPrefs.SetString ("Style", "Candy");

				break;
			case Enums.Themes.Default:
				PlayerPrefs.SetString ("Style", "Default");
				break;
			case Enums.Themes.Style1:
				PlayerPrefs.SetString ("Style", "Style1");
				break;
			case Enums.Themes.Style2:
				PlayerPrefs.SetString ("Style", "Style2");
				break;
			}

		
		}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this instance is right handed.
	/// </summary>
	/// <value><c>true</c> if this instance is right handed; otherwise, <c>false</c>.</value>
	public static bool IsRightHanded {

		get {
			return PlayerPrefs.GetInt ("RightHanded", 0) == 1;
		}

		set {

			bool paramGet = value;

			if (paramGet == true) {
				PlayerPrefs.SetInt ("RightHanded", 1);
			} else {
				PlayerPrefs.SetInt ("RightHanded", 0);
			}
		}
	}


    #region Score


    /// <summary>
    /// The score.
    /// </summary>
    public static int Score = 0;

    /// /// <summary>
    /// The score move cards.
    /// </summary>
    public const int ScoreMoveCards = 5;

    /// <summary>
    /// The score result cards.
    /// </summary>
    public const int ScoreResultCards = 10;


    #endregion

	#region Board Information

	/// <summary>
	/// Gets or sets the best score on easy.
	/// </summary>
	/// <value>The best score on easy.</value>
	public static int BestScoreOnEasy {
		get {
			return PlayerPrefs.GetInt ("BestScoreOnEasy", 0);
		}

		set{
			PlayerPrefs.SetInt ("BestScoreOnEasy", value);
		}
	}

	/// <summary>
	/// Gets or sets the best score on hard.
	/// </summary>
	/// <value>The best score on hard.</value>
	public static int BestScoreOnHard
	{
		get {
			return PlayerPrefs.GetInt ("BestScoreOnHard", 0);
		}

		set{
			PlayerPrefs.SetInt ("BestScoreOnHard", value);
		}
	}

	/// <summary>
	/// Gets or sets the best moves on easy.
	/// </summary>
	/// <value>The best moves on easy.</value>
	public static int BestMovesOnEasy
	{
		get {
			return PlayerPrefs.GetInt ("BestMovesOnEasy", 0);
		}

		set{
			PlayerPrefs.SetInt ("BestMovesOnEasy", value);
		}
	}

	/// <summary>
	/// Gets or sets the best move on hard.
	/// </summary>
	/// <value>The best move on hard.</value>
	public static int BestMoveOnHard
	{
		get {
			return PlayerPrefs.GetInt ("BestMoveOnHard", 0);
		}

		set{
			PlayerPrefs.SetInt ("BestMoveOnHard", value);
		}
	}

	/// <summary>
	/// Gets or sets the best time on easy.
	/// </summary>
	/// <value>The best time on easy.</value>
	public static float BestTimeOnEasy
	{
		get {
			return PlayerPrefs.GetFloat ("BestTimeOnEasy", 0f);
		}
		set{
			PlayerPrefs.SetFloat ("BestTimeOnEasy", value);
		}
	}

	/// <summary>
	/// Gets or sets the best time on hard.
	/// </summary>
	/// <value>The best time on hard.</value>
	public static float BestTimeOnHard
	{
		get {
			return PlayerPrefs.GetFloat ("BestTimeOnHard", 0f);
		}
		set{
			PlayerPrefs.SetFloat ("BestTimeOnHard", value);
		}
	}

	/// <summary>
	/// Gets or sets the total time on easy.
	/// </summary>
	/// <value>The total time on easy.</value>
	public static float TotalTimeOnEasy
	{
		get {
			return PlayerPrefs.GetFloat ("TotalTimeOnEasy", 0f);
		}
		set{
			PlayerPrefs.SetFloat ("TotalTimeOnEasy", value);
		}
	}

	/// <summary>
	/// Gets or sets the total time on hard.
	/// </summary>
	/// <value>The total time on hard.</value>
	public static float TotalTimeOnHard
	{
		get {
			return PlayerPrefs.GetFloat ("TotalTimeOnHard", 0f);
		}
		set{
			PlayerPrefs.SetFloat ("TotalTimeOnHard", value);
		}
	}


	#endregion



}
